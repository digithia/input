import {
  customElement,
  css,
  html,
  property,
  TemplateResult,
  state,
  CSSResult,
  query,
} from 'lit-element'
import { classMap } from 'lit-html/directives/class-map'
import { DigUtils, Direction } from '../common/dig-utils'
import commonStyle from '../common/dig-input-shared.scss'
import style from './dig-input-phone.scss'
import { DigInputElement, DigInputPhoneErrors } from '../common/dig-input'
import { countries, Country } from 'countries-list'
import { PhoneNumberUtil } from 'google-libphonenumber'
import { DigInputConfig, getCountry } from '../common/dig-input-config'
import { computeValue } from './dig-input-phone-util'

@customElement('dig-input-phone')
export class DigInputPhone extends DigInputElement {
  static styles = css`
    ${commonStyle as CSSResult}
    ${style as CSSResult}
  `

  /**
   * Input's current value. Changes within input are reflected to the DOM.
   */
  @property({ type: String, reflect: true })
  value = ''
  /**
   * Triggers required validator.
   */
  @property({ type: Boolean })
  required = false
  /**
   * Selects the default country.
   */
  @property({ type: String, attribute: 'default-country' })
  defaultCountryCode = getCountry()
  /**
   * Selects how many country suggestion to display at the same time.
   */
  @property({ type: String, attribute: 'max-shown' })
  numberSuggestionDisplayed = 6
  /**
   * Represents the time before errors will be displayed. Works only if errorBehavior is sets to 'debounce' (default)
   */
  @property({ type: Number, attribute: 'debounce-delay' })
  debounceDelay = DigInputConfig.ERROR_DELAY
  /**
   * Selects the error behavior.
   */
  @property({ type: String, attribute: 'error-behavior' })
  errorBehavior = DigInputConfig.ERROR_BEHAVIOR
  /**
   * Contains error messages which are displayed if needed.
   */
  @property({ type: Object })
  errors: DigInputPhoneErrors = {}

  @state()
  protected isSelectOpen = false
  @state()
  protected hint = ''
  @state()
  protected currentCountry?: Country

  protected currentCountryCode = ''
  protected currentCountryIndex = -1
  protected countries = Object.entries(countries).filter((c) =>
    PhoneNumberUtil.getInstance().getSupportedRegions().includes(c[0])
  )

  @property({ type: Array, attribute: 'countries' })
  availableCountries = this.countries.map((c) => c[0].toLowerCase())

  protected filteredCountries = this.countries
  protected searchValue = ''
  protected mask = ''
  protected placeholder = ''
  protected viewValue = ''
  protected isInternalChange = false
  protected errorTimeout?: NodeJS.Timeout
  protected asyncTimeout?: NodeJS.Timeout

  get currentFlag(): string {
    return this.currentCountry?.emoji || ''
  }

  get currentPhonePrefix(): string {
    return this.currentCountry?.phone || ''
  }

  get valid(): boolean {
    if (!this.inputEl) return true
    return this.inputEl.validity.valid
  }

  @query('.input-search')
  inputSearchEl!: HTMLInputElement

  @query('.dig-input-phone')
  inputEl!: HTMLInputElement

  @query('.list-flags')
  listFlagsEl!: HTMLUListElement

  render(): TemplateResult {
    return html`
      <div class="select-field">
        <div
          class="select-flags ${classMap({ open: this.isSelectOpen })}"
          @mousedown="${this.toggleSelectMenu}"
        >
          <span class="phone-flag">${this.currentFlag}</span>
          <span class="phone-prefix">+${this.currentPhonePrefix}</span>
          <input
            class="input-search"
            type="text"
            placeholder="${DigInputConfig.SEARCH_LABEL}"
            @focus="${this.inputFocused}"
            @blur="${this.inputBlured}"
            @input="${this.searchCountry}"
            @keydown="${this.handleKeydown}"
            @mousedown="${this.preventToggleMenu}"
          />
          <span class="arrow"></span>
          <ul class="list-flags" tabindex="-1">
            ${this.countries.map((country) => {
              return html`
                <li
                  @mousedown="${(e: Event) =>
                    this.clickOnSuggestion(e, country[0])}"
                  tabindex="-1"
                  class="list-item ${classMap({
                    selected: this.currentCountryCode === country[0],
                    hidden: this.hide(country[0]),
                  })}"
                >
                  <span class="list-item-flag">${country[1].emoji} </span>
                  <span class="list-item-name">${country[1].native}</span>
                  <span class="list-item-phone">(+${country[1].phone})</span>
                </li>
              `
            })}
          </ul>
        </div>
        <dig-input-field
          ?show-validity="${this.showValidity}"
          ?valid="${this.valid && !!this.value}"
          ?invalid="${!this.valid}"
          ?focused="${this.focused}"
        >
          <input
            class="dig-input-phone ${classMap({
              disabled: this.disabled || this.readonly,
            })}"
            .id="${this.id}"
            .placeholder="${this.placeholder}"
            type="tel"
            .value="${this.viewValue}"
            ?disabled="${this.disabled}"
            ?readonly="${this.readonly}"
            ?required="${this.required}"
            inputmode="phone"
            size="1"
            @input="${this.handleInput}"
            @blur="${this.handleBlur}"
            @focus="${this.handleFocus}"
            tabindex="${this.readonly ? -1 : '$1'}"
          />
          <dig-input-label
            ?asterisk="${this.required}"
            ?active="${!!this.inputEl?.value || this.focused}"
            ?transparent="${(this.disabled || this.readonly) && !this.value}"
            @toggle="${this.setFocus}"
          >
            <slot></slot>
          </dig-input-label>
          <dig-input-hint ?hidden="${this.showValidity && !this.valid}">
            ${this.hint}
          </dig-input-hint>
          <dig-input-error ?shown="${this.showValidity && !this.valid}">
            ${this.errorMessage}
          </dig-input-error>
        </dig-input-field>
      </div>
    `
  }

  firstUpdated(): void {
    this.selectCountryByCode(this.defaultCountryCode.toUpperCase())
    this.style.setProperty(
      '--dig-input-phone-number-suggestion-diplayed',
      '' + this.numberSuggestionDisplayed
    )
  }

  attributeChangedCallback(name: string, oldval: string, newval: string): void {
    super.attributeChangedCallback(name, oldval, newval)
    if (name === 'value') {
      setTimeout(() => {
        if (!this.isInternalChange) {
          this.handleValue(newval, true)
        }
        this.isInternalChange = false
      })
    } else if (name === 'countries') {
      this.countries = Object.entries(countries).filter(
        (c) =>
          PhoneNumberUtil.getInstance().getSupportedRegions().includes(c[0]) &&
          this.availableCountries.includes(c[0].toLowerCase())
      )
    } else if (name === 'default-country') {
      this.selectCountryByCode(this.defaultCountryCode.toUpperCase())
    }
  }

  protected hide(countryCode: string): boolean {
    return !this.filteredCountries.find((c) => c[0] === countryCode)
  }

  protected toggleSelectMenu(event: Event): void {
    if (this.isSelectOpen) {
      this.inputSearchEl.blur()
    } else {
      this.inputSearchEl.focus()
      setTimeout(() => {
        this.setScrollToList()
      })
    }
    event.preventDefault()
  }

  protected inputFocused(): void {
    this.isSelectOpen = true
  }

  protected inputBlured(): void {
    this.isSelectOpen = false
  }

  protected setScrollToList(direction?: Direction): void {
    const currentScroll = this.listFlagsEl.scrollTop || 0
    const itemHeight = +getComputedStyle(this)
      .getPropertyValue('--dig-input-phone-suggestion-height')
      .replace('px', '')
    const topScroll = itemHeight * this.currentCountryIndex
    const selectHeight = itemHeight * this.numberSuggestionDisplayed

    if (!direction) {
      this.listFlagsEl.scroll({
        top: topScroll,
      })
    } else if (direction === Direction.UP) {
      if (currentScroll > topScroll) {
        this.listFlagsEl.scroll({
          top: topScroll,
        })
      } else if (currentScroll + selectHeight < topScroll) {
        this.listFlagsEl.scroll({
          top: topScroll,
        })
      }
    } else if (direction === Direction.DOWN) {
      if (currentScroll + selectHeight < topScroll + itemHeight) {
        this.listFlagsEl.scroll({
          top: topScroll - selectHeight + itemHeight,
        })
      } else if (currentScroll > topScroll) {
        this.listFlagsEl.scroll({
          top: topScroll,
        })
      }
    }
  }

  protected preventToggleMenu(event: Event): void {
    event.preventDefault()
    event.stopPropagation()
  }

  protected handleInput(): void {
    this.touched = true
    this.isInternalChange = true

    let value = this.inputEl.value
    const cursorIndex = this.inputEl.selectionStart
    this.handleValue(value)
    if ((this.inputEl.selectionStart || 0) - (cursorIndex || 0) >= 2) {
      this.inputEl.selectionStart = cursorIndex
      this.inputEl.selectionEnd = cursorIndex
    }
  }

  protected handleValue(value: string, force = false) {
    const res = computeValue(value, this.currentCountryCode)
    this.selectCountryByCode(res.countryCode)
    if (this.value !== res.phoneInternational) {
      this.value = res.phoneInternational
    }
    this.hint = this.value
    this.placeholder = res.mask
    this.mask = res.mask.replace(/[0-9]/g, '9')

    if (force) {
      this.inputEl.value = res.phoneInternational
    }

    if (this.errorBehavior === 'immediate') {
      this.checkError()
    } else if (this.errorBehavior === 'debounce') {
      this.errorTimeout = DigUtils.debounce(
        this.debounceDelay,
        this.errorTimeout,
        () => this.checkError()
      )
    }
  }

  protected handleBlur(): void {
    this.focused = false
    if (this.errorBehavior === 'blur') {
      this.checkError()
    }
  }

  protected handleFocus(): void {
    this.focused = true
    if (this.errorBehavior === 'blur') {
      this.checkError()
    }
  }

  protected checkError() {
    if (this.errorBehavior === 'none') return

    this.inputEl.setCustomValidity('')
    this.errorMessage = ''

    if (this.inputEl.validity.valueMissing) {
      this.errorMessage = this.inputEl.validationMessage
    } else if (this.inputEl.value && !this.value) {
      this.inputEl.setCustomValidity(
        this.errors.invalid || 'Phone number is invalid'
      )
      this.errorMessage = this.errors.invalid || 'Phone number is invalid'
    }
  }

  protected handleKeydown(event: KeyboardEvent): void {
    if (event.key === 'ArrowDown') {
      if (this.currentCountryIndex < this.filteredCountries.length - 1) {
        this.selectCountryByIndex(this.currentCountryIndex + 1)
      } else {
        this.selectCountryByIndex(0)
      }
      this.setScrollToList(Direction.DOWN)
      this.isInternalChange = true
      this.handleValue(this.inputEl.value)
    } else if (event.key === 'ArrowUp') {
      if (this.currentCountryIndex > 0) {
        this.selectCountryByIndex(this.currentCountryIndex - 1)
      } else {
        this.selectCountryByIndex(this.filteredCountries.length - 1)
      }
      this.setScrollToList(Direction.UP)
      this.isInternalChange = true
      this.handleValue(this.inputEl.value)
    } else if (event.key === 'Enter') {
      this.isSelectOpen = false
      this.inputEl.focus()
    }
  }

  protected getCountryFromIndex(index = -1): [string, Country] | undefined {
    return this.filteredCountries.find((c, i) => index === i)
  }

  protected selectCountryByIndex(index: number): void {
    this.currentCountryIndex = index
    this.currentCountryCode =
      this.filteredCountries.find((c, i) => i === index)?.[0] || ''
    this.currentCountry = this.countries.find(
      (c) => c[0] === this.currentCountryCode
    )?.[1]
  }

  protected selectCountryByCode(countryCode: string): void {
    countryCode = countryCode.toUpperCase()
    this.currentCountryCode = countryCode
    this.currentCountryIndex = this.filteredCountries.findIndex(
      (c) => c[0] === countryCode
    )
    this.currentCountry = this.countries.find(
      (c) => c[0] === this.currentCountryCode
    )?.[1]
  }

  protected clickOnSuggestion(event: Event, countryCode: string): void {
    this.selectCountryByCode(countryCode)
    this.isInternalChange = true
    this.handleValue(this.inputEl.value)
    this.inputEl.focus()
    event.preventDefault()
    event.stopPropagation()
  }

  protected searchCountry(): void {
    const filter = this.inputSearchEl.value.toLocaleLowerCase() || ''
    this.filteredCountries = this.countries.filter(
      (country) =>
        country[0].toLocaleLowerCase().match(DigUtils.escapeRegex(filter)) ||
        country[1].name
          .toLocaleLowerCase()
          .match(DigUtils.escapeRegex(filter)) ||
        country[1].native
          .toLocaleLowerCase()
          .match(DigUtils.escapeRegex(filter)) ||
        country[1].phone
          .toLocaleLowerCase()
          .match(DigUtils.escapeRegex(filter)) ||
        country[1].emoji.toLocaleLowerCase().match(DigUtils.escapeRegex(filter))
    )
    this.selectCountryByIndex(0)
    this.setScrollToList()
  }

  public getModelValue(): string {
    return this.value
  }

  public reset(): void {
    this.value = ''
    this.hint = ''
  }

  public checkValidity(): boolean {
    this.touched = true
    this.checkError()
    return !!this.inputEl.validity.valid
  }

  getValidity(): boolean {
    return !!this.inputEl.validity.valid
  }
}
