import { DigInputMask } from '../common/dig-input-mask'
import {
  PhoneNumberUtil,
  PhoneNumber,
  PhoneNumberFormat,
} from 'google-libphonenumber'

export interface PhoneData {
  phoneNational: string
  phoneInternational: string
  countryCode: string
  mask: string
}

export const computeValue = (
  phoneValue: string,
  countryCode: string
): PhoneData => {
  const phoneUtil = PhoneNumberUtil.getInstance()
  let phoneNumber: PhoneNumber | null

  let resInternationalNumber = ''
  let resNationalNumber = ''
  let resMask = ''
  let resCountryCode = ''

  try {
    phoneNumber = phoneUtil.parse(phoneValue)
  } catch (e) {
    try {
      phoneNumber = phoneUtil.parseAndKeepRawInput(phoneValue, countryCode)
    } catch (error) {
      phoneNumber = null
    }
  }

  if (
    phoneNumber &&
    phoneUtil.isPossibleNumber(phoneNumber) &&
    phoneUtil.isValidNumber(phoneNumber)
  ) {
    const associatedCountryCode = phoneUtil.getRegionCodeForNumber(
      phoneNumber
    ) as string

    const nationalNumber = phoneUtil.format(
      phoneNumber,
      PhoneNumberFormat.NATIONAL
    )
    const internationalNumber = phoneUtil.format(
      phoneNumber,
      PhoneNumberFormat.INTERNATIONAL
    )

    const placeholder = phoneUtil.getExampleNumber(associatedCountryCode)
    const examleFormattedNumber = phoneUtil.format(
      placeholder,
      PhoneNumberFormat.NATIONAL
    )
    const mask = examleFormattedNumber

    resInternationalNumber = internationalNumber
    resNationalNumber = DigInputMask.mask(
      nationalNumber,
      mask.replace(/[0-9]/g, '9'),
      DigInputMask.REGEX_MAP
    )
    resCountryCode = associatedCountryCode
    resMask = mask
  } else {
    const placeholder = phoneUtil.getExampleNumber(countryCode)
    const examleFormattedNumber = phoneUtil.format(
      placeholder,
      PhoneNumberFormat.NATIONAL
    )
    const mask = examleFormattedNumber

    resNationalNumber = DigInputMask.mask(
      phoneValue,
      mask.replace(/[0-9]/g, '9'),
      DigInputMask.REGEX_MAP
    )
    resCountryCode = countryCode
    resMask = mask

    try {
      phoneNumber = phoneUtil.parseAndKeepRawInput(
        resNationalNumber,
        countryCode
      )
      const internationalNumber = phoneUtil.format(
        phoneNumber,
        PhoneNumberFormat.INTERNATIONAL
      )

      if (phoneUtil.isPossibleNumber(phoneNumber)) {
        resInternationalNumber = internationalNumber
      }
    } catch (e) {}
  }

  return {
    phoneInternational: resInternationalNumber,
    phoneNational: resNationalNumber,
    countryCode: resCountryCode,
    mask: resMask,
  }
}
