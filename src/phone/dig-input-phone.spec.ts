import { describe, it } from 'mocha'
import { expect } from 'chai'
import { computeValue, PhoneData } from './dig-input-phone-util'

interface Test {
  input: { countryCode: string; phoneValue: string }
  output: PhoneData
}

const testValues: Test[] = [
  {
    input: { countryCode: '', phoneValue: '' },
    output: {
      countryCode: 'FR',
      phoneInternational: '',
      phoneNational: '',
      mask: '01 23 45 67 89',
    },
  },
  {
    input: { countryCode: '', phoneValue: '+33 1 23 45 67 89' },
    output: {
      countryCode: 'FR',
      phoneInternational: '+33 1 23 45 67 89',
      phoneNational: '01 23 45 67 89',
      mask: '01 23 45 67 89',
    },
  },
  {
    input: { countryCode: '', phoneValue: '+33123456789' },
    output: {
      countryCode: 'FR',
      phoneInternational: '+33 1 23 45 67 89',
      phoneNational: '01 23 45 67 89',
      mask: '01 23 45 67 89',
    },
  },
  {
    input: { countryCode: '', phoneValue: '+49 9230 9203' },
    output: {
      countryCode: 'DE',
      phoneInternational: '+49 9230 9203',
      phoneNational: '092 309203',
      mask: '030 123456',
    },
  },
  {
    input: { countryCode: '', phoneValue: '+4992309203' },
    output: {
      countryCode: 'DE',
      phoneInternational: '+49 9230 9203',
      phoneNational: '092 309203',
      mask: '030 123456',
    },
  },
  {
    input: { countryCode: 'FR', phoneValue: '01 23 45 67 89' },
    output: {
      countryCode: 'FR',
      phoneInternational: '+33 1 23 45 67 89',
      phoneNational: '01 23 45 67 89',
      mask: '01 23 45 67 89',
    },
  },
  {
    input: { countryCode: 'FR', phoneValue: '01 23 45' },
    output: {
      countryCode: 'FR',
      phoneInternational: '',
      phoneNational: '01 23 45',
      mask: '01 23 45 67 89',
    },
  },
  {
    input: { countryCode: '', phoneValue: '01 23 45' },
    output: {
      countryCode: 'FR',
      phoneInternational: '',
      phoneNational: '01 23 45',
      mask: '01 23 45 67 89',
    },
  },
  {
    input: { countryCode: 'FR', phoneValue: '012345' },
    output: {
      countryCode: 'FR',
      phoneInternational: '',
      phoneNational: '01 23 45',
      mask: '01 23 45 67 89',
    },
  },
  {
    input: { countryCode: '', phoneValue: '012345' },
    output: {
      countryCode: 'FR',
      phoneInternational: '',
      phoneNational: '01 23 45',
      mask: '01 23 45 67 89',
    },
  },
  {
    input: { countryCode: 'DE', phoneValue: '01 23 45 67 89' },
    output: {
      countryCode: 'DE',
      phoneInternational: '+49 12345678',
      phoneNational: '012 345678',
      mask: '030 123456',
    },
  },
  {
    input: { countryCode: 'DE', phoneValue: '+33 1 23 45 67 89' },
    output: {
      countryCode: 'FR',
      phoneInternational: '+33 1 23 45 67 89',
      phoneNational: '01 23 45 67 89',
      mask: '01 23 45 67 89',
    },
  },
  {
    input: { countryCode: 'FR', phoneValue: '+33012345678' },
    output: {
      countryCode: 'FR',
      phoneInternational: '',
      phoneNational: '33 01 23 45 67',
      mask: '01 23 45 67 89',
    },
  },
]

const defaultCountryCode = 'FR'

describe(`DigInputPhone`, () => {
  describe(`computeValue`, () => {
    for (const test of testValues) {
      it(`with \nphone="${test.input.phoneValue}"\ncode="${test.input.countryCode}"\nshould return\ninternational="${test.output.phoneInternational}"\nnational="${test.output.phoneNational}"\ncountry="${test.output.countryCode}"\n`, () => {
        const res = computeValue(
          test.input.phoneValue,
          test.input.countryCode || defaultCountryCode
        )
        expect(res).to.deep.equals(test.output)
      })
    }
  })
})
