export class DigInputMask {
  static readonly ALL = '*'
  static readonly REGEXP_ALL = /./
  static readonly ALPHA = 'A'
  static readonly REGEXP_ALPHA = /[^\d\W]/
  static readonly NUMBER = '9'
  static readonly REGEXP_NUMBER = /\d/
  static readonly ALPHANUMERIC = '?'
  static readonly REGEXP_ALPHANUMERIC = /\w|\d/
  static readonly NUMERIC = '8'
  static readonly REGEXP_NUMERIC = /[\d\.\,\-]/

  static readonly REGEX_MAP = new Map([
    [DigInputMask.ALL, DigInputMask.REGEXP_ALL],
    [DigInputMask.ALPHA, DigInputMask.REGEXP_ALPHA],
    [DigInputMask.NUMBER, DigInputMask.REGEXP_NUMBER],
    [DigInputMask.ALPHANUMERIC, DigInputMask.REGEXP_ALPHANUMERIC],
    [DigInputMask.NUMERIC, DigInputMask.REGEXP_NUMERIC],
  ])

  static mask(
    value: string,
    mask: string,
    regexMap: Map<string, RegExp> = DigInputMask.REGEX_MAP
  ): string {
    if (!mask || !value) {
      return value
    }
    let len = value.length
    const maskLen = mask.length
    let pos = 0
    let newValue = ''

    for (let i = 0; i < Math.min(len, maskLen); i++) {
      const maskChar = mask.charAt(i)
      const newChar = value.charAt(pos)
      const regex = regexMap.get(maskChar)

      if (regex) {
        pos++

        if (regex.test(newChar)) {
          newValue += newChar
        } else {
          i--
          len--
        }
      } else {
        if (maskChar === newChar) {
          pos++
        } else {
          len++
        }

        newValue += maskChar
      }
    }
    return newValue
  }

  static unmask(
    maskedValue: string,
    mask: string,
    regexMap: Map<string, RegExp> = DigInputMask.REGEX_MAP
  ): string {
    if (!mask) {
      return maskedValue
    }
    const maskLen = (mask && mask.length) || 0
    return maskedValue
      .split('')
      .filter((currChar, idx) => idx < maskLen && regexMap.has(mask[idx]))
      .join('')
  }
}
