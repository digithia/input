export interface KeyValue {
  key: string | number
  value: string
}

export enum Direction {
  UP = 'up',
  DOWN = 'down',
  RIGHT = 'right',
  LEFT = 'left',
}

export class DigUtils {
  static generateUniqueId(): string {
    return `${Math.random()}`
  }

  static firstLetterUppercase(str: string): string {
    if (!str) return ''
    return str.charAt(0).toUpperCase() + str.slice(1)
  }

  /**
   * Transforms a string into a valid RegExp
   */
  static escapeRegex(str: string): RegExp {
    return new RegExp(str.replace(/([.?*+^$[\]\\(){}|-])/g, '\\$1'))
  }

  /**
   * Sets a debounce time before execute callback.
   * @param ms The time you want to wait.
   * @param timeout The current timeout to cancel to prevent multiple calls.
   * @param callback The callback function you want to execute
   */
  static debounce(
    ms: number,
    timeout: NodeJS.Timeout | undefined,
    callback: () => void
  ): NodeJS.Timeout {
    if (timeout) {
      clearTimeout(timeout)
    }

    return setTimeout(() => {
      callback()
    }, ms)
  }

  /**
   * Checks if an IBAN (International Bank Account Number) is valid or not. Returns true if no IBAN is provided.
   *
   * @param iban The IBAN string you want to check
   *
   * There are some valid IBAN :
   * - FR76 3000 6000 0112 3456 7890 189
   * - GB82 WEST 1234 5698 7654 32
   * - DE89 3704 0044 0532 0130 00
   */
  static checkIBAN(iban: string): boolean {
    if (!iban) return true
    const trimIBAN = iban.replace(/ /g, '')
    if (trimIBAN.length < 15 || trimIBAN.length > 34) {
      return false
    }
    const reversedIBAN = `${trimIBAN.slice(4, trimIBAN.length)}${trimIBAN.slice(
      0,
      4
    )}`
    let integerIBAN = ''
    for (const char of reversedIBAN) {
      if (isNaN(+char)) {
        integerIBAN += (char.codePointAt(0) || 0) - 55
      } else {
        integerIBAN += char
      }
    }

    if (BigInt(integerIBAN) % BigInt(97) === BigInt(1)) {
      return true
    } else {
      return false
    }
  }

  static isMobile(): boolean {
    return !!(
      /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ||
      (navigator.userAgent.match(/Mac/) &&
        navigator.maxTouchPoints &&
        navigator.maxTouchPoints > 2)
    )
  }
}
