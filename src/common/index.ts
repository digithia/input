import { DigInputArrow } from './field/dig-input-arrow'
import { DigInputClose } from './field/dig-input-close'
import { DigInputError } from './field/dig-input-error'
import { DigInputField } from './field/dig-input-field'
import { DigInputFixedText } from './field/dig-input-fixed-text'
import { DigInputHint } from './field/dig-input-hint'
import { DigInputLabel } from './field/dig-input-label'
import { DigInputShowPassword } from './field/dig-input-show-password'
import { DigInputSelection } from './selection/dig-input-selection'

customElements.define('dig-input-selection', DigInputSelection)
customElements.define('dig-input-field', DigInputField)
customElements.define('dig-input-label', DigInputLabel)
customElements.define('dig-input-hint', DigInputHint)
customElements.define('dig-input-error', DigInputError)
customElements.define('dig-input-fixed-text', DigInputFixedText)
customElements.define('dig-input-show-password', DigInputShowPassword)
customElements.define('dig-input-arrow', DigInputArrow)
customElements.define('dig-input-close', DigInputClose)

export * from './dig-input-config'
export * from './dig-input'
export * from './field/dig-input-field'
export * from './field/dig-input-label'
export * from './field/dig-input-hint'
export * from './field/dig-input-error'
export * from './field/dig-input-fixed-text'
export * from './field/dig-input-show-password'
export * from './field/dig-input-arrow'
export * from './field/dig-input-close'
export * from './selection/dig-input-selection'
