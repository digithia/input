import {
  LitElement,
  css,
  html,
  property,
  TemplateResult,
  CSSResult,
  query,
} from 'lit-element'
import { classMap } from 'lit-html/directives/class-map'
import { DigUtils, Direction } from '../dig-utils'

import commonStyle from '../dig-input-shared.scss'
import style from './dig-input-selection.scss'
import { DigInputSelectableElement } from '../dig-input'

export class DigInputSelection extends LitElement {
  static styles = css`
    ${commonStyle as CSSResult}
    ${style as CSSResult}
  `

  @property({ attribute: 'max-shown', type: Number })
  maxShown = 5

  @property({ type: Boolean, reflect: true })
  toggled = false

  @property({ type: Number })
  selected = 0

  itemsEl: DigInputSelectableElement[] = []

  @query('slot')
  slotEl!: HTMLSlotElement

  @query('.dig-input-selection')
  listEl!: HTMLUListElement

  get visibleItemsEl(): DigInputSelectableElement[] {
    return this.itemsEl.filter((el) => el.visible)
  }

  render(): TemplateResult {
    return html`
      <ul
        class="dig-input-selection ${classMap({ open: this.toggled })}"
        tabindex="-1"
      >
        <slot @slotchange="${this.slotChange}"></slot>
      </ul>
    `
  }

  slotChange() {
    this.itemsEl = (this.slotEl?.assignedElements() ||
      []) as DigInputSelectableElement[]

    if (this.itemsEl[0] instanceof HTMLSlotElement) {
      this.itemsEl = (this.itemsEl[0] as any).assignedElements() || []
    }

    for (const item of this.itemsEl) {
      item.removeEventListener('mousedown', this.handleClick)
      item.addEventListener('mousedown', this.handleClick)
    }

    this.style.setProperty(
      '--dig-input-selection-item-height',
      `${this.itemsEl[0]?.clientHeight}px`
    )
  }

  goUp() {
    if (this.selected <= 0) {
      this.selected = this.visibleItemsEl.length - 1
    } else {
      this.selected--
    }
    this.setScrollToList(Direction.UP)
    this.validateSelection()
  }

  goDown() {
    if (this.selected >= this.visibleItemsEl.length - 1) {
      this.selected = 0
    } else {
      this.selected++
    }
    this.setScrollToList(Direction.DOWN)
    this.validateSelection()
  }

  validateSelection(): void {
    const item = this.visibleItemsEl.find(
      (el, index) => index === this.selected
    )
    this.handleItemValidated(item)
  }

  protected handleClick = (e: MouseEvent): void => {
    let target = e.target as HTMLElement
    while (target && !target.id) {
      target = target.parentElement as HTMLElement
    }
    this.handleItemValidated(target as DigInputSelectableElement)
    this.close()
  }

  handleItemValidated = (selectedItem?: DigInputSelectableElement): void => {
    let index = 0
    for (const item of this.visibleItemsEl) {
      if (item.id === selectedItem?.id) {
        item.selected = true
        this.selected = index
      } else {
        item.selected = false
      }
      index++
    }
    this.dispatchEvent(new CustomEvent('validate', { detail: selectedItem }))
  }

  close() {
    if (this.toggled) this.toggled = false
  }

  open(currentValue?: string) {
    if (!this.toggled) this.toggled = true
    this.search('')
    if (currentValue) {
      this.selectItemByValue(currentValue)
    } else {
      this.selectItemByIndex(0)
    }
  }

  firstUpdated(): void {
    setTimeout(() => {
      this.selectItemByIndex(this.selected)
    })
  }

  search(value: string): void {
    const filter = value.toLocaleLowerCase() || ''
    for (const option of this.itemsEl) {
      if (
        option.textContent
          ?.toLocaleLowerCase()
          .match(DigUtils.escapeRegex(filter))
      ) {
        option.setAttribute('visible', 'visible')
      } else {
        option.removeAttribute('visible')
      }
    }
    // this.open()
  }

  attributeChangedCallback(name: string, oldval: string, newval: string): void {
    super.attributeChangedCallback(name, oldval, newval)
    if (name === 'max-shown') {
      this.style.setProperty(
        '--dig-input-selection-max-shown',
        '' + this.maxShown
      )
    } else if (name === 'selected') {
      this.selectItemByIndex(this.selected)
    }
  }

  selectItemByIndex(index: number) {
    let i = 0
    for (const item of this.visibleItemsEl) {
      if (i === index) {
        item.selected = true
      } else {
        item.selected = false
      }
      i++
    }
  }

  selectItemByValue(value: string) {
    for (const item of this.visibleItemsEl) {
      if (item.value === value) {
        item.selected = true
      } else {
        item.selected = false
      }
    }
  }

  protected setScrollToList(direction?: Direction): void {
    const currentScroll = this.listEl.scrollTop || 0
    const itemHeight = +getComputedStyle(this)
      .getPropertyValue('--dig-input-selection-item-height')
      .replace('px', '')
    const topScroll = itemHeight * this.selected
    const selectHeight = itemHeight * this.maxShown

    if (!direction) {
      this.listEl.scroll({
        top: topScroll,
      })
    } else if (direction === Direction.UP) {
      if (currentScroll > topScroll) {
        this.listEl.scroll({
          top: topScroll,
        })
      } else if (currentScroll + selectHeight < topScroll) {
        this.listEl.scroll({
          top: topScroll,
        })
      }
    } else if (direction === Direction.DOWN) {
      if (currentScroll + selectHeight < topScroll + itemHeight) {
        this.listEl.scroll({
          top: topScroll - selectHeight + itemHeight,
        })
      } else if (currentScroll > topScroll) {
        this.listEl.scroll({
          top: topScroll,
        })
      }
    }
  }
}
