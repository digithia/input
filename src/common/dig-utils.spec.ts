import { expect } from 'chai'
import { describe, it } from 'mocha'
import '../index.spec'
import { DigUtils } from './dig-utils'

describe('DigUtils', () => {
  describe('firstLetterUppercase', () => {
    it('should return string with first letter uppercased', () => {
      expect(DigUtils.firstLetterUppercase('my text')).to.equal('My text')
    })
    it('should return empty string on invalid param', () => {
      expect(DigUtils.firstLetterUppercase('')).to.equal('')
    })
  })

  describe('checkIBAN', () => {
    it('should return false on invalid IBAN', () => {
      expect(DigUtils.checkIBAN('notaniban')).to.be.false
      expect(DigUtils.checkIBAN('FR32 0948 0239 8402 93')).to.be.false
      expect(DigUtils.checkIBAN('GB234973229')).to.be.false
      expect(DigUtils.checkIBAN('FR76 3000 6000 0112 3456 7890 188')).to.be
        .false
    })
    it('should return true if no IBAN is provided', () => {
      expect(DigUtils.checkIBAN('')).to.be.true
    })
    it('should return true on valid IBAN', () => {
      expect(DigUtils.checkIBAN('FR76 3000 6000 0112 3456 7890 189')).to.be.true
      expect(DigUtils.checkIBAN('GB82 WEST 1234 5698 7654 32')).to.be.true
      expect(DigUtils.checkIBAN('DE89 3704 0044 0532 0130 00')).to.be.true
      expect(DigUtils.checkIBAN('GR16 0110 1250 0000 0001 2300 695')).to.be.true
      expect(DigUtils.checkIBAN('BE68 5390 0754 7034')).to.be.true
      expect(DigUtils.checkIBAN('FR14 2004 1010 0505 0001 3M02 606')).to.be.true
      expect(DigUtils.checkIBAN('IT60 X054 2811 1010 0000 0123 456')).to.be.true
      expect(DigUtils.checkIBAN('LI21 0881 0000 2324 013A A')).to.be.true
      expect(DigUtils.checkIBAN('MT84 MALT 0110 0001 2345 MTLC AST0 01S')).to.be
        .true
      expect(DigUtils.checkIBAN('LU28 0019 4006 4475 0000')).to.be.true
    })
  })
})
