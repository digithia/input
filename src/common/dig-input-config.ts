import { DigUtils } from './dig-utils'

export type ErrorTrigger = 'none' | 'immediate' | 'debounce' | 'blur'
const supportedLanguages = ['en', 'fr']

export const getLang = () => {
  let lang = 'fr'
  if (navigator.languages != undefined) lang = navigator.languages[0]
  else lang = navigator.language

  if (lang.match('-')) lang = lang.split('-')[0]

  if (supportedLanguages.includes(lang)) return lang
  return 'fr'
}

export const getCountry = () => {
  let lang = 'gb'
  if (navigator.languages != undefined) lang = navigator.languages[0]
  else lang = navigator.language

  if (lang.match('-')) lang = lang.split('-')[1]

  return lang || 'gb'
}

const loadLocale = (lang: string) => {
  return require(`./locales/${lang}.json`)
}

const locale = loadLocale(getLang())

export class DigInputConfig {
  static ERROR_DELAY: number = 300
  static ERROR_BEHAVIOR: ErrorTrigger = 'debounce'
  static ERROR_MESSAGE: string = DigUtils.firstLetterUppercase(
    locale.invalidInput
  )
  static SEARCH_LABEL: string = DigUtils.firstLetterUppercase(locale.search)
}
