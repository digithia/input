import {
  customElement,
  LitElement,
  css,
  html,
  TemplateResult,
  CSSResult,
} from 'lit-element'
import { classMap } from 'lit-html/directives/class-map'

import commonStyle from './common/dig-input-shared.scss'
import style from './dig-input-example.scss'

@customElement('dig-input-example')
export class DigInputExample extends LitElement {
  static styles = css`
    ${commonStyle as CSSResult}
    ${style as CSSResult}
  `

  render(): TemplateResult {
    return html`
      <div>
        <slot></slot>
      </div>
    `
  }

  firstUpdated(): void {}

  attributeChangedCallback(name: string, oldval: string, newval: string): void {
    super.attributeChangedCallback(name, oldval, newval)
  }
}
