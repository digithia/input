import {
  LitElement,
  css,
  html,
  TemplateResult,
  CSSResult,
  property,
} from 'lit-element'
import { classMap } from 'lit-html/directives/class-map'

import commonStyle from '../dig-input-shared.scss'
import style from './dig-input-hint.scss'

export class DigInputHint extends LitElement {
  static styles = css`
    ${commonStyle as CSSResult}
    ${style as CSSResult}
  `

  @property({ type: Boolean })
  hidden = false

  @property({ type: String })
  position = 'left'

  render(): TemplateResult {
    return html`
      <div
        class="dig-input-hint ${classMap({
          hidden: this.hidden,
          [this.position]: true,
        })}"
      >
        <slot></slot>
      </div>
    `
  }

  firstUpdated(): void {}

  attributeChangedCallback(name: string, oldval: string, newval: string): void {
    super.attributeChangedCallback(name, oldval, newval)
  }
}
