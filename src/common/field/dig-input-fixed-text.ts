import {
  LitElement,
  css,
  html,
  TemplateResult,
  CSSResult,
  property,
} from 'lit-element'
import { classMap } from 'lit-html/directives/class-map'

import commonStyle from '../dig-input-shared.scss'
import style from './dig-input-fixed-text.scss'

export class DigInputFixedText extends LitElement {
  static styles = css`
    ${commonStyle as CSSResult}
    ${style as CSSResult}
  `

  @property({ type: Boolean })
  shown = false

  @property({ type: String })
  position = 'left'

  render(): TemplateResult {
    return html`
      <div
        class="dig-input-fixed-text ${classMap({
          shown: this.shown,
          [this.position]: true,
        })}"
      >
        <span>
          <slot></slot>
        </span>
      </div>
    `
  }

  firstUpdated(): void {}

  attributeChangedCallback(name: string, oldval: string, newval: string): void {
    super.attributeChangedCallback(name, oldval, newval)
  }
}
