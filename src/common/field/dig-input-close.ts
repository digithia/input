import { LitElement, css, html, TemplateResult, CSSResult } from 'lit-element'

import commonStyle from '../dig-input-shared.scss'
import style from './dig-input-close.scss'

export class DigInputClose extends LitElement {
  static styles = css`
    ${commonStyle as CSSResult}
    ${style as CSSResult}
  `

  render(): TemplateResult {
    return html` <div class="dig-input-close"></div> `
  }

  firstUpdated(): void {}

  attributeChangedCallback(name: string, oldval: string, newval: string): void {
    super.attributeChangedCallback(name, oldval, newval)
  }
}
