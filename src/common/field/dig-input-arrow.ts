import {
  LitElement,
  css,
  html,
  TemplateResult,
  CSSResult,
  property,
} from 'lit-element'
import { classMap } from 'lit-html/directives/class-map'

import commonStyle from '../dig-input-shared.scss'
import style from './dig-input-arrow.scss'

export class DigInputArrow extends LitElement {
  static styles = css`
    ${commonStyle as CSSResult}
    ${style as CSSResult}
  `

  @property({ type: Boolean })
  toggled = false

  render(): TemplateResult {
    return html`
      <div class="dig-input-arrow ${classMap({ toggled: this.toggled })}">
        <slot></slot>
      </div>
    `
  }

  firstUpdated(): void {}

  attributeChangedCallback(name: string, oldval: string, newval: string): void {
    super.attributeChangedCallback(name, oldval, newval)
  }
}
