import {
  LitElement,
  css,
  html,
  TemplateResult,
  CSSResult,
  property,
} from 'lit-element'
import { classMap } from 'lit-html/directives/class-map'

import commonStyle from '../dig-input-shared.scss'
import style from './dig-input-show-password.scss'

export class DigInputShowPassword extends LitElement {
  static styles = css`
    ${commonStyle as CSSResult}
    ${style as CSSResult}
  `

  @property({ type: Boolean })
  shown = false

  @property({ type: Boolean })
  toggled = false

  render(): TemplateResult {
    return html` <div
      class="dig-input-show-password ${classMap({
        shown: this.shown,
      })}"
    >
      <div
        class="dig-input-show-password-button ${classMap({
          toggled: this.toggled,
        })}"
        @click="${this.toggle}"
      >
        <div class="dig-input-show-password-line"></div>
      </div>
    </div>`
  }

  toggle() {
    this.toggled = !this.toggled
    this.dispatchEvent(new CustomEvent('toggle'))
  }

  firstUpdated(): void {}

  attributeChangedCallback(name: string, oldval: string, newval: string): void {
    super.attributeChangedCallback(name, oldval, newval)
  }
}
