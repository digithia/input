import {
  LitElement,
  css,
  html,
  TemplateResult,
  CSSResult,
  property,
} from 'lit-element'
import { classMap } from 'lit-html/directives/class-map'

import commonStyle from '../dig-input-shared.scss'
import style from './dig-input-field.scss'

export class DigInputField extends LitElement {
  static styles = css`
    ${commonStyle as CSSResult}
    ${style as CSSResult}
  `

  @property({ type: Boolean, attribute: 'show-validity' })
  showValidity = false
  @property({ type: Boolean })
  valid = false
  @property({ type: Boolean })
  invalid = false
  @property({ type: Boolean })
  focused = false
  @property({ type: Boolean })
  disabled = false
  @property({ type: Boolean })
  readonly = false

  render(): TemplateResult {
    return html`
      <div
        class="dig-input-field ${classMap({
          'show-validity': this.showValidity,
          valid: this.valid,
          invalid: this.invalid,
          focused: this.focused,
          disabled: this.disabled,
          readonly: this.readonly,
        })}"
      >
        <slot></slot>
      </div>
    `
  }

  firstUpdated(): void {}

  attributeChangedCallback(name: string, oldval: string, newval: string): void {
    super.attributeChangedCallback(name, oldval, newval)
  }
}
