import {
  LitElement,
  css,
  html,
  TemplateResult,
  CSSResult,
  property,
} from 'lit-element'
import { classMap } from 'lit-html/directives/class-map'

import commonStyle from '../dig-input-shared.scss'
import style from './dig-input-label.scss'

export class DigInputLabel extends LitElement {
  static styles = css`
    ${commonStyle as CSSResult}
    ${style as CSSResult}
  `

  @property({ type: Boolean })
  active = false

  @property({ type: Boolean })
  asterisk = false

  @property({ type: Boolean })
  transparent = false

  render(): TemplateResult {
    return html`
      <label
        class="dig-input-label ${classMap({
          active: this.active,
          transparent: this.transparent,
        })}"
        @click="${this.handleClick}"
      >
        <slot></slot>
        ${this.asterisk ? html`<span class="asterisk"> *</span>` : ''}
      </label>
    `
  }

  firstUpdated(): void {}

  attributeChangedCallback(name: string, oldval: string, newval: string): void {
    super.attributeChangedCallback(name, oldval, newval)
  }

  handleClick(): void {
    this.dispatchEvent(new CustomEvent('toggle'))
  }
}
