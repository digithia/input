import { state, LitElement, property } from 'lit-element'
import { DigUtils } from './dig-utils'

/**
 * DigInputElement doc
 */
export abstract class DigInputElement extends LitElement {
  /**
   * Contains the current value of this input.
   */
  abstract value: any
  /**
   * Represents the HTML input element.
   */
  abstract inputEl: HTMLElement

  /**
   * The current id of this input. Generated randomly if not explicitely sets.
   */
  @property({ type: String, reflect: true })
  id = DigUtils.generateUniqueId()
  /**
   * Sets input to a disabled state.
   */
  @property({ type: Boolean })
  disabled = false
  /**
   * Sets input to a readonly state.
   */
  @property({ type: Boolean })
  readonly = false

  /**
   * Indicates wheter this input has focus or not.
   */
  @state()
  focused: boolean = false
  /**
   * Contains the current error message.
   */
  @state()
  errorMessage: string = ''
  /**
   * Indicates wether this input is touched or not.
   */
  @state()
  touched: boolean = false

  /**
   * Indicates wheter the input validity color should be displayed or not.
   */
  protected get showValidity(): boolean {
    return (!!this.value || this.touched) && !this.disabled && !this.readonly
  }

  /**
   * Gets the model value of this input.
   */
  getModelValue(): any {
    return this.value
  }
  /**
   * Resets this input with an empty value and no errors.
   */
  reset(): void {
    this.value = ''
    this.touched = false
  }
  /**
   * Sets this input as untouched (hide errors)
   */
  public setUntouched(): void {
    this.touched = false
  }
  /**
   * Sets focus on this input field.
   */
  setFocus(): void {
    this.inputEl?.focus()
  }
  /**
   * Gets input validity
   */
  getValidity(): boolean {
    return true
  }
  /**
   * Gets input validity and shows errors if there is one
   */
  checkValidity(): boolean {
    return true
  }
}

export abstract class DigInputWrapperElement extends LitElement {
  /**
   * Contains the current value of this input.
   */
  abstract value: any

  /**
   * The current id of this input. Generated randomly if not explicitely sets by the user.
   */
  @property({ type: String, reflect: true })
  id = DigUtils.generateUniqueId()
  /**
   * Sets input to a disabled state.
   */
  @property({ type: Boolean })
  disabled = false
  /**
   * Sets input to a readonly state.
   */
  @property({ type: Boolean })
  readonly = false

  /**
   * Gets the model value of this input.
   */
  abstract getModelValue(): any
  /**
   * Resets this input with an empty value.
   */
  abstract reset(): void
  /**
   * Sets focus on this input field.
   */
  abstract setFocus(): void
}

export interface DigInputTogglableElement extends HTMLElement {
  /**
   * Sets element to checked.
   */
  check(): void
  /**
   * Sets element to unchecked.
   */
  uncheck(): void
  /**
   * Switchs checked state. If not checked, it becomes checked. If checked, it becomes unchecked.
   */
  toggle(): void
  /**
   * Gets the current checked state of this element
   */
  isChecked(): boolean
  value: string
}

export interface DigInputSelectableElement extends HTMLElement {
  id: string
  selected: boolean
  visible: boolean
  value: string
  label: string
}

/**
 * Represents the errors messages of an input
 */
export interface DigInputErrors {
  required?: string
  pattern?: string
  maxlength?: string
  minlength?: string
  min?: string
  max?: string
  notANumber?: string
  validator?: string
}

/**
 * Represents the errors messages of a phone input
 */
export interface DigInputPhoneErrors {
  required?: string
  invalid?: string
}

/**
 * Use this class to performs operations on several inputs at once.
 */
export class DigInputController {
  /**
   * Performs a check validity of all inputs contained into the provided parent.
   * Error will appears if exists.
   *
   * @param selector The html parent selector of the inputs you want to checks.
   * @returns true if all inputs are valid and false otherwise
   */
  static checkInputsValidity(selector: string): boolean {
    const inputs = document.querySelectorAll(`${selector} *`)
    let valid = true
    for (const i in inputs) {
      const input = inputs[i] as DigInputElement
      if (
        input &&
        input.tagName?.toLowerCase().match(/^dig-input-*/) &&
        input.checkValidity &&
        input.checkValidity() === false
      ) {
        valid = false
      }
    }
    return valid
  }

  /**
   * Restes the validity of all inputs contained into the provided parent.
   * All inputs will be marked as untouched and no error will be displayed.
   *
   * @param selector The html parent selector of the inputs you want to reset.
   */
  static resetInputsValidity(selector: string): void {
    const inputs = document.querySelectorAll(`${selector} *`)
    for (const i in inputs) {
      const input = inputs[i] as DigInputElement
      if (
        input &&
        input.tagName?.toLowerCase().match(/^dig-input-*/) &&
        input.reset
      ) {
        input.reset()
      }
    }
  }

  /**
   * Gets the validity of all inputs contained into the provided parent.
   * Error will not appears if they exist.
   *
   * @param selector The html parent selector of the inputs you want to analyse.
   * @returns true if all inputs are valid and false otherwise
   */
  static getInputsValidity(selector: string): boolean {
    const inputs = document.querySelectorAll(`${selector} *`)
    let valid = true
    for (const i in inputs) {
      if (!valid) break
      const input = inputs[i] as DigInputElement
      if (
        input &&
        input.tagName?.toLowerCase().match(/^dig-input-*/) &&
        input.getValidity
      ) {
        valid = input.getValidity()
      }
    }
    return valid
  }
}
