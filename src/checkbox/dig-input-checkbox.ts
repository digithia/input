import {
  customElement,
  css,
  html,
  property,
  TemplateResult,
  CSSResult,
  query,
} from 'lit-element'
import { classMap } from 'lit-html/directives/class-map'
import { ifDefined } from 'lit-html/directives/if-defined'

import commonStyle from '../common/dig-input-shared.scss'
import style from './dig-input-checkbox.scss'
import { DigInputElement, DigInputTogglableElement } from '../common/dig-input'

@customElement('dig-input-checkbox')
export class DigInputCheckbox
  extends DigInputElement
  implements DigInputTogglableElement
{
  static styles = css`
    ${commonStyle as CSSResult}
    ${style as CSSResult}
  `

  /**
   * Checkbox's current value. Changes within input are reflected to the DOM.
   */
  @property({ type: String, reflect: true })
  value = ''
  /**
   * Checkbox's current state. Changes within input are reflected to the DOM.
   */
  @property({ type: Boolean, reflect: true })
  checked = false
  /**
   * Enables free input text so the user can type whatever he wants.
   */
  @property({ type: Boolean, attribute: 'is-text-input' })
  isTextInput = false
  /**
   * Default value for text-input.
   */
  @property({ type: String, attribute: 'default-value' })
  defaultValue = ''

  @query('.checkbox')
  inputEl!: HTMLInputElement
  @query('.input-text')
  inputTextEl!: HTMLInputElement
  @query('.label')
  labelEl!: HTMLLabelElement

  render(): TemplateResult {
    return html`<div class="field ${classMap({ disabled: this.disabled })}">
      <input
        .id="${this.id}"
        class="checkbox ${classMap({ 'checkbox-text': this.isTextInput })}"
        type="checkbox"
        ?checked="${this.checked}"
        .value="${ifDefined(this.value)}"
        ?disabled="${this.disabled}"
        @input="${this.setInputTextFocus}"
      />
      <div class="checkmark" @click="${this.handleClick}"></div>
      <label class="label" for="${this.id}" @click="${this.handleClick}">
        <slot></slot>
      </label>
      ${this.isTextInput
        ? html` <input
            class="input-text"
            .placeholder="${this.defaultValue}"
            @input="${this.changeTextInput}"
          />`
        : ''}
    </div>`
  }

  firstUpdated(): void {
    if (!this.defaultValue) {
      this.defaultValue = this.value
    }
  }

  handleClick(event: MouseEvent) {
    event?.preventDefault()
    this.toggle()
  }

  toggle(): void {
    this.inputEl.checked = !this.inputEl.checked
    if (this.inputEl.checked) {
      this.checked = true
      this.setInputTextFocus()
    } else {
      this.checked = false
    }

    this.dispatchEvent(new CustomEvent('change', { detail: this.checked }))
  }

  public check(): void {
    this.inputEl.checked = true
    this.checked = true
  }

  public uncheck(): void {
    this.inputEl.checked = false
    this.checked = false
  }

  public isChecked(): boolean {
    return this.inputEl?.checked
  }

  protected setInputTextFocus(): void {
    if (this.isTextInput) {
      this.inputTextEl.focus()
    }
  }

  protected changeTextInput(): void {
    if (!this.isTextInput) return

    if (this.inputTextEl.value) {
      this.inputEl.value = this.inputTextEl.value
      this.labelEl.innerText = this.inputTextEl.value
      this.value = this.inputTextEl.value
    } else {
      this.inputEl.value = this.defaultValue
      this.labelEl.innerText = this.defaultValue
      this.value = this.defaultValue
    }
  }

  public getModelValue(): string | null {
    return this.checked ? this.value : null
  }
}
