import {
  customElement,
  css,
  html,
  property,
  TemplateResult,
  CSSResult,
  query,
} from 'lit-element'

import commonStyle from '../common/dig-input-shared.scss'
import style from './dig-input-checkbox-wrapper.scss'
import { DigInputWrapperElement } from '../common/dig-input'
import { DigInputCheckbox } from './dig-input-checkbox'

@customElement('dig-input-checkbox-wrapper')
export class DigInputCheckboxWrapper extends DigInputWrapperElement {
  static styles = css`
    ${commonStyle as CSSResult}
    ${style as CSSResult}
  `
  /**
   * Checkbox wrapper's current value.
   * Contains an array of all checked checkbox within the wrapper.
   */
  @property({ type: Array, reflect: true })
  value: any = []
  /**
   * Contains the title of the checkbox wrapper, displayed above the checkboxes.
   */
  @property({ type: String })
  label: string = ''
  /**
   * Sets all checkboxes to a disabled state.
   */
  @property({ type: Boolean })
  disabled = false
  /**
   * Sets a maximum checkbox you can checked.
   */
  @property({ type: Number, attribute: 'max' })
  maxCheckedCount = 0
  /**
   * Sets a minimum checkbox you can checked.
   */
  @property({ type: Number, attribute: 'min' })
  minCheckedCount = 0

  protected checkboxesEl: DigInputCheckbox[] = []
  protected internalChange: boolean = false
  protected selected = 0

  @query('slot')
  slotEl!: HTMLSlotElement

  render(): TemplateResult {
    return html`
      ${this.label ? html`<h6 class="title">${this.label}</h6>` : ''}
      <div class="checkbox-wrapper">
        <slot @slotchange="${this.slotChange}"></slot>
      </div>
    `
  }

  protected slotChange() {
    this.checkboxesEl = (this.slotEl?.assignedElements() ||
      []) as DigInputCheckbox[]
    this.internalChange = true

    setTimeout(() => {
      this.refreshValue()
    })

    this.checkboxesEl.forEach((checkboxEl) => {
      checkboxEl.removeEventListener('change', this.handleChanges)
      checkboxEl.removeEventListener('input', this.handleChanges)
      checkboxEl.addEventListener('change', this.handleChanges)
      checkboxEl.addEventListener('input', this.handleChanges)
    })
  }

  protected handleChanges = () => {
    this.internalChange = true
    this.refreshValue()
    this.dispatchEvent(new CustomEvent('change', { detail: this.value }))
  }

  protected refreshValue() {
    const value: string[] = []
    for (const checkboxEl of this.checkboxesEl) {
      if (checkboxEl.isChecked()) {
        value.push(checkboxEl.value)
      }
    }
    if (
      this.value.length !== value.length ||
      !this.value.every((v: string, i: number) => v === value[i])
    ) {
      this.value = value
    }
  }

  protected handleMaxCheckedCount() {
    if (this.maxCheckedCount && this.value.length >= this.maxCheckedCount) {
      this.checkboxesEl.forEach((checkbox) => {
        if (!checkbox.isChecked()) {
          checkbox.setAttribute('disabled', '')
        }
      })
    } else {
      this.checkboxesEl.forEach((checkbox) => {
        if (!checkbox.isChecked()) {
          checkbox.removeAttribute('disabled')
        }
      })
    }
  }

  protected handleMinCheckedCount() {
    if (this.minCheckedCount && this.value.length < this.minCheckedCount) {
      this.checkboxesEl.forEach((checkbox) => {
        checkbox.setAttribute('required', '')
      })
    } else {
      this.checkboxesEl.forEach((checkbox) => {
        checkbox.removeAttribute('required')
      })
    }
  }

  protected setValue(value: string[]) {
    this.value = value

    for (const checkboxEl of this.checkboxesEl) {
      if (this.value.find((v: string) => checkboxEl.value === v)) {
        checkboxEl.check()
      } else {
        checkboxEl.uncheck()
      }
    }
  }

  attributeChangedCallback(name: string, oldval: string, newval: string) {
    super.attributeChangedCallback(name, oldval, newval)

    if (name === 'value') {
      setTimeout(() => {
        if (!this.internalChange) {
          this.setValue(JSON.parse(newval))
          this.internalChange = true
        } else {
          this.internalChange = false
        }

        this.handleMaxCheckedCount()
        this.handleMinCheckedCount()
      })
    }
  }

  public reset(): void {
    this.value = []
  }

  public getModelValue(): any {
    return this.value
  }

  public setFocus(): void {
    this.checkboxesEl[0]?.focus()
  }
}
