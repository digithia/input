import {
  customElement,
  css,
  html,
  TemplateResult,
  CSSResult,
  query,
  property,
} from 'lit-element'
import { classMap } from 'lit-html/directives/class-map'
import { ifDefined } from 'lit-html/directives/if-defined'

import commonStyle from '../common/dig-input-shared.scss'
import style from './dig-input-textarea.scss'
import { DigInputElement, DigInputErrors } from '../common/dig-input'
import { DigInputConfig } from '../common/dig-input-config'
import { DigUtils } from '../common/dig-utils'

/**
 * Represents a custom textarea input field.
 * Extends DigInputElement and thus LitElement.
 * This custom Component handles text input with severals line.
 * It can be configured with severals behaviors.
 *
 * @example
 * Here's a simple HTML example :
 * ```
 * <dig-input-textarea value="">My input textarea</dig-input-textarea>
 * ```
 * @example
 * Here's an example with some attributes :
 * ```
 * <dig-input-textarea value="something" required autogrow="false" maxlength="200">
 *   My input textarea
 * </dig-input-textarea>
 * ```
 */
@customElement('dig-input-textarea')
export class DigInputTextarea extends DigInputElement {
  static styles = css`
    ${commonStyle as CSSResult}
    ${style as CSSResult}
  `

  /**
   * Input's current value. Changes within input are reflected to the DOM.
   */
  @property({ type: String, reflect: true })
  value: string = ''
  /**
   * Contains the placeholder shown within input if there is no text written and input is focused.
   */
  @property({ type: String })
  placeholder = ''
  /**
   * Triggers required validator.
   */
  @property({ type: Boolean })
  required = false
  /**
   * Triggers pattern validator
   */
  @property({ type: String })
  pattern = /[0-9]*/
  /**
   * Triggers maxlength validator
   */
  @property({ type: Number })
  maxlength = undefined
  /**
   * Triggers minlength validator
   */
  @property({ type: Number })
  minlength = undefined
  /**
   * Show an hint below the input field
   */
  @property({ type: String })
  hint = ''
  /**
   * Input's name. Use it in order to trigger browsers heuristics for autocompletion.
   */
  @property({ type: String })
  name = ''
  /**
   * Contains error messages which are displayed if needed.
   */
  @property({ type: Object })
  errors: DigInputErrors = {}
  /**
   * Selects the error behavior.
   */
  @property({ type: String, attribute: 'error-behavior' })
  errorBehavior = DigInputConfig.ERROR_BEHAVIOR
  /**
   * Add a custom validator to the input.
   */
  @property({ type: Function })
  validator?: (value: string) => boolean = undefined
  /**
   * Add a custom async validator to the input
   */
  @property({ type: Function })
  asyncValidator?: (value: string) => Promise<boolean> | boolean = undefined
  /**
   * Represents the time before errors will be displayed. Works only if errorBehavior is sets to 'debounce' (default)
   */
  @property({ type: Number, attribute: 'debounce-delay' })
  debounceDelay = DigInputConfig.ERROR_DELAY
  /**
   * Represents the time before the async validator is executed.
   */
  @property({ type: Number, attribute: 'async-debounce-delay' })
  asyncDebounceDelay = DigInputConfig.ERROR_DELAY
  /**
   * Activates or not the autogrow of the textarea on user input.
   */
  @property({ type: Boolean, attribute: 'autogrow' })
  enableAutogrow = true

  protected errorTimeout?: NodeJS.Timeout
  protected asyncTimeout?: NodeJS.Timeout

  valid = true

  @query('.dig-input-textarea')
  inputEl!: HTMLTextAreaElement

  @query('.dig-input-grow-wrap')
  growWrapEl!: HTMLDivElement

  @query('.dig-input-field')
  fieldEl!: HTMLDivElement

  render(): TemplateResult {
    return html`
      <dig-input-field
        ?show-validity="${this.showValidity}"
        ?valid="${this.valid && !!this.value}"
        ?invalid="${!this.valid}"
        ?focused="${this.focused}"
        class="dig-input-field"
      >
        <div class="dig-input-grow-wrap">
          <textarea
            .id="${this.id}"
            .value="${ifDefined(this.value)}"
            .name="${this.name}"
            .placeholder="${this.focused ? this.placeholder : ''}"
            ?disabled="${this.disabled}"
            ?readonly="${this.readonly}"
            ?required="${this.required}"
            .pattern=${ifDefined(this.pattern)}
            .minLength=${ifDefined(this.minlength)}
            .maxLength=${ifDefined(this.maxlength)}
            class="dig-input-textarea ${classMap({
              disabled: this.disabled || this.readonly,
            })}"
            @input="${this.handleInput}"
            @blur="${this.handleBlur}"
            @focus="${this.handleFocus}"
          ></textarea>
        </div>
        <dig-input-label
          ?asterisk="${this.required}"
          ?active="${!!this.value || this.focused}"
          ?transparent="${(this.disabled || this.readonly) && !this.value}"
          @toggle="${this.setFocus}"
        >
          <slot></slot>
        </dig-input-label>
        <dig-input-hint ?hidden="${this.showValidity && !this.valid}">
          ${this.hint}
        </dig-input-hint>
        <dig-input-error ?shown="${this.showValidity && !this.valid}">
          ${this.errorMessage}
        </dig-input-error>
        <dig-input-hint
          class="maxlength"
          ?hidden="${this.maxlength === undefined}"
          position="right"
        >
          ${this.value?.length || 0} / ${this.maxlength}
        </dig-input-hint>
        <div class="dig-input-grab" @mousedown="${this.triggerGrab}"></div>
      </dig-input-field>
    `
  }

  grabbed = false

  protected triggerGrab = (e: MouseEvent) => {
    this.grabbed = true
    document.addEventListener('mousemove', this.handleGrab)
    document.addEventListener('mouseup', this.disableGrab)
  }

  protected disableGrab = (e: MouseEvent) => {
    this.grabbed = false
    document.removeEventListener('mousemove', this.handleGrab)
    document.removeEventListener('mouseup', this.disableGrab)
  }

  protected handleGrab = (e: MouseEvent) => {
    if (!this.grabbed) return
    this.growWrapEl.style.height = `${
      e.clientY - this.growWrapEl.getBoundingClientRect().top + 4
    }px`
  }

  attributeChangedCallback(name: string, oldval: string, newval: string): void {
    super.attributeChangedCallback(name, oldval, newval)

    if (name === 'value') {
      setTimeout(() => {
        this.growWrapEl.dataset.value = this.inputEl?.value
      })
    }
  }

  protected handleInput(): void {
    this.touched = true
    this.handleValue()
  }

  protected handleBlur(): void {
    this.focused = false
    if (this.errorBehavior === 'blur') {
      this.checkError()
    }
  }

  protected handleFocus(): void {
    this.focused = true
  }

  protected handleValue(): void {
    this.value = this.inputEl?.value || ''
    this.growWrapEl.dataset.value = this.inputEl?.value
    this.valid = true
    this.checkError()
    this.requestUpdate()
  }

  protected checkError(): void {
    if (this.errorBehavior === 'none') return

    let errorMessage = null

    if (this.inputEl?.validity.valueMissing) {
      errorMessage = this.errors.required
    } else if (this.inputEl?.validity.patternMismatch) {
      errorMessage = this.errors.pattern
    } else if (this.inputEl?.validity.rangeUnderflow) {
      errorMessage = this.errors.min
    } else if (this.inputEl?.validity.rangeOverflow) {
      errorMessage = this.errors.max
    } else if (this.inputEl?.validity.tooShort) {
      errorMessage = this.errors.minlength
    } else if (this.inputEl?.validity.tooLong) {
      errorMessage = this.errors.maxlength
    } else if (this.validator) {
      if (!this.validator(this.inputEl?.value || '')) {
        this.inputEl?.setCustomValidity(
          this.errors.validator || DigInputConfig.ERROR_MESSAGE
        )
      } else {
        this.inputEl?.setCustomValidity('')
      }
    } else if (this.asyncValidator) {
      this.inputEl?.setCustomValidity('')
      this.asyncTimeout = DigUtils.debounce(
        this.asyncDebounceDelay,
        this.asyncTimeout,
        async () => {
          if (
            this.asyncValidator &&
            !(await this.asyncValidator(this.inputEl?.value || ''))
          ) {
            this.inputEl?.setCustomValidity(
              this.errors.validator || DigInputConfig.ERROR_MESSAGE
            )
          } else {
            this.inputEl?.setCustomValidity('')
          }
          this.errorMessage = this.inputEl?.validationMessage
          this.valid = this.inputEl?.validity.valid
        }
      )
    }

    this.errorMessage = errorMessage || this.inputEl?.validationMessage
    this.valid = this.inputEl?.validity.valid
  }

  public getModelValue(): string {
    return this.value
  }

  public reset(): void {
    this.value = ''
    this.valid = true
    this.touched = false
  }

  public getValidity(): boolean {
    return this.valid
  }

  public checkValidity(): boolean {
    this.touched = true
    this.checkError()
    return this.valid
  }
}
