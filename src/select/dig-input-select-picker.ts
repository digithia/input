import {
  customElement,
  css,
  html,
  property,
  TemplateResult,
  CSSResult,
  query,
  state,
} from 'lit-element'
import { classMap } from 'lit-html/directives/class-map'

import commonStyle from '../common/dig-input-shared.scss'
import style from './dig-input-select-picker.scss'
import { DigInputElement, DigInputSelectableElement } from '../common/dig-input'
import { DigInputSelection } from '../common/selection'
import { DigUtils } from '../common/dig-utils'
import { DigInputConfig } from '../common/dig-input-config'

/**
 * Represents a custom select input field.
 * Extends DigInputElement and thus LitElement.
 * This custom component displays a select picker with severals options.
 * In order to use this component you should includes in it some components that implements DigInputSelectableElement.
 * DigInputSelectPickerOption is the default one provided by this lib.
 * It can be configured with severals behaviors.
 *
 * @example Here is a simple HTML example :
 * ```html
 * <dig-input-select-picker label="My select input">
 *   ...
 * </dig-input-select-picker>
 * ```
 * @example
 * Here is an example with some attributes :
 * ```html
 * <dig-input-select-picker label="My select input" required free-value max-shown="10">
 *   ...
 * </dig-input-select-picker>
 * ```
 */
@customElement('dig-input-select-picker')
export class DigInputSelectPicker extends DigInputElement {
  static styles = css`
    ${commonStyle as CSSResult}
    ${style as CSSResult}
  `

  /**
   * Select's name.
   */
  @property({ type: String })
  name = ''
  /**
   * Select's label displayed in field.
   */
  @property({ type: String })
  label = ''
  /**
   * Select's current value. Changes within input are reflected to the DOM.
   */
  @property({ type: String, reflect: true })
  value: any = ''
  /**
   * Triggers required validator.
   */
  @property({ type: Boolean })
  required = false
  /**
   * True if select is open, false otherwise.
   */
  @property({ type: Boolean, reflect: true })
  toggled = false
  /**
   * Specifies the number of suggestion displayed at the same time within select.
   */
  @property({ attribute: 'max-shown', type: Number })
  maxShown = 5
  /**
   * Specifies the number of suggestion displayed at the same time within select.
   */
  @property({ attribute: 'auto-position', type: Boolean })
  autoPosition = true
  /**
   * If sets to true, user can type a custom value into select.
   */
  @property({ type: Boolean, attribute: 'free-value' })
  enableFreeValue = false
  /**
   * Contains the placeholder shown within select.
   */
  @property({ type: String })
  placeholder = ''

  @state()
  inputValue: string = ''

  @state()
  belowSelection: boolean = true

  optionsEl!: DigInputSelectableElement[]
  internalChange = false

  get valid(): boolean {
    return !!this.value
  }

  get invalid(): boolean {
    return !this.value && this.required
  }

  protected get showValidity(): boolean {
    return (!!this.value || this.touched) && !this.disabled && !this.readonly
  }

  @query('.dig-input-select-picker-search')
  inputEl!: HTMLInputElement

  @query('slot')
  slotEl?: HTMLSlotElement

  @query('dig-input-selection')
  selectionEl?: DigInputSelection

  render(): TemplateResult {
    return html`
      <dig-input-field
        class="dig-input-select-picker ${classMap({
      disabled: this.disabled || this.readonly,
    })}"
        ?show-validity="${this.showValidity}"
        ?valid="${this.valid}"
        ?invalid="${this.invalid}"
        ?focused="${this.focused}"
        @click="${this.openSelection}"
      >
        <input
          class="dig-input-select-picker-search ${classMap({
      disabled: this.disabled || this.readonly,
      'cannot-be-focused': !this.toggled,
    })}"
          type="text"
          id="${this.id}"
          .value="${this.inputValue}"
          .placeholder="${this.disabled || this.readonly || !this.focused
        ? ''
        : this.placeholder || DigInputConfig.SEARCH_LABEL}"
          ?required="${this.required}"
          ?readonly="${this.readonly}"
          ?disabled="${this.disabled}"
          size="5"
          autocomplete="off"
          @keyup="${this.handleKeyUp}"
          @keydown="${this.handleKeyDown}"
          @input="${this.handleInput}"
          @focus="${this.focusSearchInput}"
        />
        <dig-input-label
          ?asterisk="${this.required}"
          ?active="${!!this.value || this.focused}"
          ?transparent="${(this.disabled || this.readonly) && !this.value}"
        >
          ${this.label}
        </dig-input-label>
        <dig-input-close
          @click="${() => this.reset()}"
          ?hidden="${!(this.focused && this.value) || this.required}"
        ></dig-input-close>
        <dig-input-arrow ?toggled="${this.toggled}"></dig-input-arrow>
      </dig-input-field>
      <div class="selection">
        <dig-input-selection
          class="${classMap({
          below: this.belowSelection,
          above: !this.belowSelection,
        })}"
          ?toggled="${this.toggled}"
          @validate="${this.itemSelected}"
          max-shown="${this.maxShown}"
        >
          <slot @slotchange="${this.slotChange}"></slot>
        </dig-input-selection>
      </div>
    `
  }

  slotChange() {
    this.optionsEl = (this.slotEl?.assignedElements() ||
      []) as DigInputSelectableElement[]
  }

  attributeChangedCallback(name: string, oldval: string, newval: string): void {
    super.attributeChangedCallback(name, oldval, newval)
    if (name === 'value') {
      setTimeout(() => {
        if (this.internalChange) {
          return (this.internalChange = false)
        }
        if (this.enableFreeValue) {
          this.value = newval
          this.inputValue = newval
        } else {
          const option = this.optionsEl.find((o) => o.value === newval)
          if (option) {
            this.value = option.value
            this.inputValue = option.label || option.innerText || ''
          } else {
            this.value = ''
            this.inputValue = ''
          }
        }
        this.dispatchEvent(new CustomEvent('select', { detail: this.value }))
      })
    }
  }

  protected openSelection = () => {
    this.selectionEl?.open(this.value)
    this.toggled = true
    this.focused = true
    this.touched = true
    document.addEventListener('click', this.closeSelection)
    if (!DigUtils.isMobile()) {
      this.inputEl.focus()
    }
    setTimeout(() => {
      if (this.autoPosition) {
        this.belowSelection =
          window.innerHeight - this.getBoundingClientRect().bottom >
          (this.selectionEl?.clientHeight || 0)
      } else {
        this.belowSelection = true
      }
    })
  }

  protected closeSelection = (event: MouseEvent) => {
    if (!event.composedPath().includes(this.shadowRoot as EventTarget)) {
      this.selectionEl?.close()
      this.focused = false
      this.toggled = false
      document.removeEventListener('click', this.closeSelection)
    }
  }

  protected itemSelected(event: CustomEvent): void {
    this.inputValue = event.detail?.label || event.detail?.innerText || ''
    this.value = event.detail?.value || ''
    this.internalChange = true
    this.focused = false
    this.dispatchEvent(new CustomEvent('select', { detail: this.value }))
  }

  protected focusSearchInput(): void {
    if (!this.focused) {
      this.openSelection()
    }
  }

  protected handleKeyUp(event: KeyboardEvent) {
    if (event.key === 'ArrowDown') {
      this.selectionEl?.goDown()
    } else if (event.key === 'ArrowUp') {
      this.selectionEl?.goUp()
    } else if (event.key === 'Enter') {
      if (!this.value) {
        this.selectionEl?.validateSelection()
      }
      this.selectionEl?.close()
      this.toggled = false
    } else if (event.key === 'Escape') {
      this.selectionEl?.close()
      this.toggled = false
      document.removeEventListener('click', this.closeSelection)
    } else if (event.key === 'Tab') {
      this.openSelection()
    }
  }

  protected handleKeyDown(event: KeyboardEvent) {
    if (event.key === 'Tab') {
      this.selectionEl?.close()
      this.focused = false
      this.toggled = false
      document.removeEventListener('click', this.closeSelection)
    }
  }

  protected handleInput(event: InputEvent) {
    this.internalChange = true
    const target = event.target as HTMLInputElement
    this.inputValue = target.value || ''
    if (this.enableFreeValue) {
      this.value = target.value
    } else {
      const option = this.optionsEl.find(
        (o) =>
          target.value &&
          (o.label === target.value || o.innerText === target.value)
      )
      if (option) {
        this.value = option.value
      } else {
        this.value = ''
      }
    }
    this.dispatchEvent(new CustomEvent('select', { detail: this.value }))
    this.selectionEl?.search(this.inputValue)
  }

  public getModelValue(): string {
    return this.value
  }

  public reset(): void {
    this.internalChange = true
    this.touched = false
    this.value = ''
    this.dispatchEvent(new CustomEvent('select', { detail: this.value }))
    this.inputValue = ''
  }

  public checkValidity(): boolean {
    this.touched = true
    return this.getValidity()
  }

  public getValidity(): boolean {
    return !this.required || !!this.value
  }
}
