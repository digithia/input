import {
  customElement,
  LitElement,
  css,
  html,
  property,
  TemplateResult,
  CSSResult,
  query,
} from 'lit-element'
import { classMap } from 'lit-html/directives/class-map'

import commonStyle from '../common/dig-input-shared.scss'
import style from './dig-input-select-picker-option.scss'
import { DigUtils } from '../common/dig-utils'
import { DigInputSelectableElement } from '../common/dig-input'

/**
 * Represents the default option field you want to put into your select picker.
 * Extends LitElement and DigInputSelectableElement.
 *
 * @example Here is a simple HTML example :
 * ```html
 * <dig-input-select-picker ...>
 *   <dig-input-select-picker-option value="1">
 *     My first choice
 *   </dig-input-select-picker-option>
 *   ...
 * </dig-input-select-picker>
 *
 * ```
 * @example
 * Here is a more complexe example :
 * ```html
 * <dig-input-select-picker ...>
 *   <dig-input-select-picker-option value="1" label="First">
 *     <span>1 : </span>
 *     <span>My first <strong>choice<strong></span>
 *   </dig-input-select-picker-option>
 *   ...
 * </dig-input-select-picker>
 * ```
 */
@customElement('dig-input-select-picker-option')
export class DigInputSelectPickerOption
  extends LitElement
  implements DigInputSelectableElement {
  static styles = css`
    ${commonStyle as CSSResult}
    ${style as CSSResult}
  `

  /**
   * Represents the label shown into select when option is selected.
   * Could be different than the label specified into the slot.
   * If unsets, the label into the slot element will be used.
   */
  @property({ reflect: true, type: String })
  label = ''
  /**
   * Represents the model value of the option.
   * This attribute is used to sets the select picker value.
   */
  @property({ reflect: true, type: String })
  value = ''
  /**
   * Indicates if the options is visible or not.
   */
  @property({ reflect: true, type: Boolean })
  visible = true
  /**
   * Indicates if the options is selected or not.
   */
  @property({ reflect: true, type: Boolean })
  selected = true
  /**
   * The current id of this option. Generated randomly if not explicitely sets.
   */
  @property({ reflect: true, type: String })
  id = DigUtils.generateUniqueId()

  @query('slot')
  slotEl?: HTMLSlotElement

  render(): TemplateResult {
    return html`
      <li
        class="dig-ui-select-option ${classMap({
          selected: this.selected,
          visible: this.visible,
        })}"
      >
        <slot></slot>
      </li>
    `
  }
}
