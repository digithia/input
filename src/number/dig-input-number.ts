import {
  customElement,
  css,
  html,
  property,
  TemplateResult,
  CSSResult,
  query,
  state,
} from 'lit-element'
import { classMap } from 'lit-html/directives/class-map'
import { ifDefined } from 'lit-html/directives/if-defined'
import { DigUtils } from '../common/dig-utils'

import commonStyle from '../common/dig-input-shared.scss'
import style from './dig-input-number.scss'
import { DigInputElement, DigInputErrors } from '../common/dig-input'
import { DigInputConfig } from '../common/dig-input-config'
import { DigInputMask } from '../common/dig-input-mask'

const DEFAULT_ERROR_MESSAGE = 'Number input is invalid'

@customElement('dig-input-number')
export class DigInputNumber extends DigInputElement {
  static styles = css`
    ${commonStyle as CSSResult}
    ${style as CSSResult}
  `
  /**
   * Input's current value. Changes within input are reflected to the DOM.
   */
  @property({ type: Number, reflect: true })
  value: number | null = null
  /**
   * Input's type. Each HTML input's type will works but you should use DigInputNumber to handle numbers.
   */
  @property({ type: String })
  name = ''
  /**
   * Contains the placeholder shown within input if there is no text written and input is focused.
   */
  @property({ type: String })
  placeholder = ' '
  /**
   * Triggers required validator.
   */
  @property({ type: Boolean })
  required = false
  /**
   * Triggers pattern validator
   */
  @property({ type: String })
  pattern?: string = undefined
  /**
   * Triggers min validator
   */
  @property({ type: Number })
  min?: number = undefined
  /**
   * Triggers max validator
   */
  @property({ type: Number })
  max?: number = undefined
  /**
   * Show an hint below the input field
   */
  @property({ type: String })
  hint = ''
  /**
   * Triggers HTML step.
   */
  @property({ type: Number })
  step = 1
  /**
   * Contains error messages which are displayed if needed.
   */
  @property({ type: Object })
  errors: DigInputErrors = {}
  /**
   * Selects the error behavior.
   */
  @property({ type: String, attribute: 'error-behavior' })
  errorBehavior = DigInputConfig.ERROR_BEHAVIOR
  /**
   * Add a custom validator to the input.
   */
  @property({ type: Function })
  validator?: (value: string) => boolean = undefined
  /**
   * Add a custom async validator to the input
   */
  @property({ type: Function })
  asyncValidator?: (value: string) => Promise<boolean> | boolean = undefined
  /**
   * Represents the time before errors will be displayed. Works only if errorBehavior is sets to 'debounce' (default)
   */
  @property({ type: Number })
  debounceDelay = DigInputConfig.ERROR_DELAY
  /**
   * Represents the time before the async validator is executed.
   */
  @property({ type: Number })
  asyncDebounceDelay = DigInputConfig.ERROR_DELAY

  /**
   * Disables negative numbers.
   */
  @property({ type: Boolean, attribute: 'disabled-negative' })
  disabledNegative = false
  /**
   * Disables decimal numbers.
   */
  @property({ type: Boolean, attribute: 'disabled-decimal' })
  disabledDecimal = false
  /**
   * Specifies a maximum decimal number.
   */
  @property({ type: Number, attribute: 'decimal-count' })
  decimalCount = 9
  /**
   * Contains the prefix that will be shown before the input field.
   */
  @property({ type: String })
  prefix = ''
  /**
   * Contains the suffix that will be shown after the input field.
   */
  @property({ type: String })
  suffix = ''

  @state()
  focused = false

  protected errorTimeout?: NodeJS.Timeout
  protected asyncTimeout?: NodeJS.Timeout
  protected isInternalChange = false

  get showValidity(): boolean {
    return (
      (this.hasValue || this.touched || !!this.inputEl?.value) &&
      !this.disabled &&
      !this.readonly
    )
  }

  get hasValue(): boolean {
    return !!this.inputEl?.value
  }

  get valid(): boolean {
    if (!this.inputEl) return true
    return this.inputEl?.validity.valid
  }

  @query('.dig-input-number')
  inputEl!: HTMLInputElement

  render(): TemplateResult {
    return html`
      <dig-input-field
        ?show-validity="${this.showValidity}"
        ?valid="${this.valid && this.hasValue}"
        ?invalid="${!this.valid}"
        ?focused="${this.focused}"
      >
        <dig-input-fixed-text
          class="prefix"
          position="left"
          ?shown="${(this.hasValue || this.focused) && this.prefix}"
        >
          ${this.prefix}
        </dig-input-fixed-text>
        <input
          .id="${this.id}"
          .placeholder="${this.placeholder}"
          ?disabled="${this.disabled}"
          ?readonly="${this.readonly}"
          ?required="${this.required}"
          .pattern=${ifDefined(this.pattern)}
          .min=${ifDefined(this.min)}
          .max=${ifDefined(this.max)}
          .step="${this.step}"
          inputmode="decimal"
          type="text"
          .name="${this.name}"
          class="dig-input-number ${classMap({
            disabled: this.disabled || this.readonly,
          })}"
          size="5"
          @input="${this.handleInput}"
          @blur="${this.handleBlur}"
          @focus="${this.handleFocus}"
          @keypress="${this.handleKeypress}"
          tabindex="${this.readonly ? -1 : '$1'}"
        />
        <dig-input-fixed-text
          class="suffix"
          position="right"
          ?shown="${(this.hasValue || this.focused) && this.suffix}"
        >
          ${this.suffix}
        </dig-input-fixed-text>
        <dig-input-label
          ?asterisk="${this.required}"
          ?active="${this.hasValue || this.focused || this.inputEl?.value}"
          @toggle="${this.setFocus}"
          ?transparent="${(this.disabled || this.readonly) && !this.value}"
        >
          <slot></slot>
        </dig-input-label>
        <dig-input-hint ?hidden="${this.showValidity && !this.valid}">
          ${this.hint}
        </dig-input-hint>
        <dig-input-error ?shown="${this.showValidity && !this.valid}">
          ${this.errorMessage}
        </dig-input-error>
      </dig-input-field>
    `
  }

  firstUpdated(): void {
    this.checkError()
  }

  attributeChangedCallback(name: string, oldval: string, newval: string): void {
    super.attributeChangedCallback(name, oldval, newval)
    if (name === 'value') {
      setTimeout(() => {
        if (!this.isInternalChange) {
          this.inputEl.value = this.value as any
        }
        this.isInternalChange = false
        this.handleValue()
        this.inputEl.setCustomValidity('')
        this.requestUpdate()
        this.handleError()
      }, 0)
    }
  }

  protected handleBlur(): void {
    this.focused = false
    if (this.errorBehavior === 'blur') {
      this.checkError()
    }
  }

  protected handleFocus(): void {
    this.focused = true
  }

  protected handleKeypress(event: KeyboardEvent): void {
    if (!event.key.match(DigInputMask.REGEXP_NUMERIC)) {
      event.preventDefault()
      event.stopPropagation()
    }
  }

  protected handleInput(): void {
    this.touched = true
    this.isInternalChange = true
    this.handleValue()
  }

  protected handleValue(): void {
    this.inputEl.value = this.inputEl.value.replace(',', '.')
    const viewValue = this.inputEl.value
    let value
    if (viewValue && !isNaN(+viewValue)) {
      value = +viewValue
    } else {
      value = null
    }

    if (this.disabledNegative && value && value < 0) {
      value = Math.abs(value)
      this.inputEl.value = '' + Math.abs(+this.inputEl.value)
    }
    if (this.disabledDecimal && value) {
      value = +value.toFixed(0)
      this.inputEl.value = value.toFixed(0)
    }
    if (!isNaN(this.decimalCount) && value) {
      value = +value.toFixed(this.decimalCount)
      const parts = this.inputEl.value.split('.')
      if (parts[1]) {
        parts[1] = parts[1].slice(0, this.decimalCount)
        this.inputEl.value = parts.join('.')
      }
    }

    if (this.value !== value) {
      this.value = value
    } else {
      this.checkError()
    }
  }

  protected handleError(): void {
    if (this.errorBehavior === 'immediate') {
      this.checkError()
    } else if (this.errorBehavior === 'debounce') {
      this.errorTimeout = DigUtils.debounce(
        this.debounceDelay,
        this.errorTimeout,
        () => this.checkError()
      )
    }
  }

  protected isRangeUnderflow(): boolean {
    if (!isNaN(this.value as any) && !isNaN(this.min as any)) {
      if ((this.value || 0) < (this.min || 0)) {
        return true
      }
    }
    return false
  }

  protected isRangeOverflow(): boolean {
    if (!isNaN(this.value as any) && !isNaN(this.max as any)) {
      if ((this.value || 0) > (this.max || 0)) {
        return true
      }
    }
    return false
  }

  protected checkError(): void {
    if (this.errorBehavior === 'none') return
    this.inputEl.setCustomValidity('')

    let errorMessage = null
    if (this.inputEl?.validity.valueMissing) {
      errorMessage = this.errors.required
    } else if (this.inputEl?.validity.badInput) {
      errorMessage = this.errors.notANumber
    } else if (this.inputEl?.validity.patternMismatch) {
      errorMessage = this.errors.pattern
    } else if (
      this.inputEl?.validity.rangeUnderflow ||
      this.isRangeUnderflow()
    ) {
      errorMessage = this.errors.min
      this.inputEl.setCustomValidity(
        this.errors.validator || DEFAULT_ERROR_MESSAGE
      )
    } else if (this.inputEl?.validity.rangeOverflow || this.isRangeOverflow()) {
      errorMessage = this.errors.max
      this.inputEl.setCustomValidity(
        this.errors.validator || DEFAULT_ERROR_MESSAGE
      )
    } else if (this.validator) {
      if (!this.validator(this.inputEl?.value || '')) {
        this.inputEl.setCustomValidity(
          this.errors.validator || DEFAULT_ERROR_MESSAGE
        )
      } else {
        this.inputEl.setCustomValidity('')
      }
    } else if (this.asyncValidator) {
      this.inputEl.setCustomValidity('')
      this.asyncTimeout = DigUtils.debounce(
        this.asyncDebounceDelay,
        this.asyncTimeout,
        async () => {
          if (
            this.asyncValidator &&
            !(await this.asyncValidator(this.inputEl?.value || ''))
          ) {
            this.inputEl.setCustomValidity(
              this.errors.validator || DEFAULT_ERROR_MESSAGE
            )
          } else {
            this.inputEl.setCustomValidity('')
          }
          this.errorMessage = this.inputEl?.validationMessage
          this.requestUpdate()
        }
      )
    } else {
      if (isNaN(this.value || NaN) && this.value !== null && this.value !== 0) {
        this.inputEl.setCustomValidity(
          this.errors.notANumber || DEFAULT_ERROR_MESSAGE
        )
      } else if (this.value === null && this.inputEl?.value) {
        this.inputEl.setCustomValidity(
          this.errors.notANumber || DEFAULT_ERROR_MESSAGE
        )
      } else {
        this.inputEl.setCustomValidity('')
      }
    }

    this.errorMessage = errorMessage || this.inputEl?.validationMessage
    this.requestUpdate()
  }

  public reset(): void {
    this.value = null
  }

  public getModelValue(): number | null {
    return this.value
  }

  public setFocus(): void {
    this.inputEl?.focus()
  }

  public checkValidity(): boolean {
    this.touched = true
    this.checkError()
    return !!this.inputEl?.validity.valid
  }

  public getValidity(): boolean {
    return !!this.inputEl?.validity.valid
  }
}
