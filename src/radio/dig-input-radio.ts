import {
  customElement,
  css,
  html,
  property,
  TemplateResult,
  CSSResult,
  query,
  state,
} from 'lit-element'
import { classMap } from 'lit-html/directives/class-map'

import commonStyle from '../common/dig-input-shared.scss'
import style from './dig-input-radio.scss'
import { DigInputElement, DigInputTogglableElement } from '../common/dig-input'

/**
 *
 */
@customElement('dig-input-radio')
export class DigInputRadio
  extends DigInputElement
  implements DigInputTogglableElement
{
  static styles = css`
    ${commonStyle as CSSResult}
    ${style as CSSResult}
  `

  /**
   * Radio input's free text value.
   */
  @property({ reflect: true, type: String })
  value!: string
  /**
   * Radio's value.
   */
  @property({ type: String })
  key: string = ''
  /**
   * Enables possibility to unselect a radio button.
   */
  @property({ type: Boolean })
  unselect = false
  /**
   * Indicates the current checked state of the radio button.
   */
  @property({ type: Boolean, reflect: true })
  checked = false
  /**
   * Enables a text input for the user, so he cans type in a custom value.
   */
  @property({ type: Boolean, attribute: 'is-text-input' })
  isTextInput = false

  @state()
  isLabelHover = false

  @query('slot')
  slotEl!: HTMLSlotElement
  @query('.dig-input-radio')
  radioEl!: HTMLInputElement
  @query('.dig-input-radio-input-text')
  inputEl!: HTMLInputElement

  render(): TemplateResult {
    return html`
      <div class="dig-input-radio-field">
        <input
          class="dig-input-radio ${classMap({
            'dig-input-radio-text': this.isTextInput,
          })}"
          type="radio"
          .value="${this.key}"
          .id="${this.id}"
          @keyup="${this.handleKeyboardSelection}"
        />
        <div
          class="checkmark ${classMap({ 'laber-hover': this.isLabelHover })}"
          @click="${this.toggle}"
        ></div>
        <label
          class="label"
          @click="${this.toggle}"
          @mouseenter="${() => (this.isLabelHover = true)}"
          @mouseleave="${() => (this.isLabelHover = false)}"
        >
          <slot></slot>
        </label>
        ${this.isTextInput
          ? html` <input
              class="dig-input-radio-input-text"
              .value="${this.value}"
              placeholder="${this.value}"
              @input="${this.inputChange}"
              @blur="${() =>
                this.dispatchEvent(
                  new CustomEvent('toggle', { detail: this.radioEl.checked })
                )}"
            />`
          : ''}
      </div>
    `
  }

  firstUpdated() {
    if (this.checked) {
      this.check()
    }
  }

  protected inputChange(): void {
    if (this.inputEl.value) {
      this.radioEl.value = this.inputEl.value
      this.value = this.inputEl.value
    }
  }

  protected handleKeyboardSelection(event: KeyboardEvent) {
    if (event.key === ' ') {
      this.toggle()
    }
  }

  public getModelValue(): any {
    return this.radioEl?.checked ? this.value : null
  }

  public check(): void {
    this.radioEl.checked = true
    this.checked = true
  }

  public uncheck(): void {
    this.radioEl.checked = false
    this.checked = false
  }

  public toggle(): void {
    if (!this.radioEl) return

    if (this.radioEl?.checked) {
      if (this.unselect) {
        this.uncheck()
      }
    } else {
      this.check()
      if (this.isTextInput) {
        this.inputEl.focus()
      }
    }

    this.dispatchEvent(
      new CustomEvent('toggle', { detail: this.radioEl.checked })
    )
  }

  public isChecked(): boolean {
    return this.radioEl?.checked
  }

  public setFocus(): void {
    this.radioEl?.focus()
  }

  public reset(): void {}
}
