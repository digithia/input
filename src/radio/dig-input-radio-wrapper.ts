import {
  customElement,
  css,
  html,
  property,
  TemplateResult,
  CSSResult,
  query,
  state,
} from 'lit-element'
import { classMap } from 'lit-html/directives/class-map'

import commonStyle from '../common/dig-input-shared.scss'
import style from './dig-input-radio-wrapper.scss'
import {
  DigInputWrapperElement,
  DigInputTogglableElement,
} from '../common/dig-input'

@customElement('dig-input-radio-wrapper')
export class DigInputRadioWrapper extends DigInputWrapperElement {
  static styles = css`
    ${commonStyle as CSSResult}
    ${style as CSSResult}
  `

  /**
   * Radio wrapper's current value.
   * Contains the value of the DigInputTogglableElement if one is checked, null otherwise
   */
  @property({ reflect: true })
  value: any = null
  /**
   * Contains the title of the radio wrapper, displayed above the radio buttons.
   */
  @property({ type: String })
  label: string = ''
  /**
   * Enables possibility to unselect all radio buttons contains into wrapper.
   */
  @property({ type: Boolean })
  unselect: boolean = false
  /**
   * Triggers required validator.
   */
  @property({ type: Boolean })
  required: boolean = false

  @state()
  valid = true
  @state()
  touched = false

  protected radiosEl: DigInputTogglableElement[] = []

  @query('slot')
  slotEl!: HTMLSlotElement

  get showValidity(): boolean {
    return this.touched
  }

  render(): TemplateResult {
    return html`
      ${this.label
        ? html`<h6 class="title">
            ${this.label}
            ${this.required ? html`<span class="asterisk"> *</span>` : ''}
          </h6>`
        : ''}
      <div
        class="radio-wrapper ${classMap({
          invalid: this.showValidity && !this.valid,
        })}"
      >
        <slot @slotchange="${this.slotChange}"></slot>
      </div>
    `
  }

  protected slotChange() {
    this.radiosEl = (this.slotEl?.assignedElements() ||
      []) as DigInputTogglableElement[]

    setTimeout(() => {
      this.refreshValue()
    })

    this.radiosEl.forEach((radioEl) => {
      radioEl.removeEventListener('toggle', this.handleChanges)
      radioEl.addEventListener('toggle', this.handleChanges)

      if (this.unselect) {
        radioEl.setAttribute('unselect', '')
      }
    })
  }

  protected handleChanges = (event: Event) => {
    const target = event.target as DigInputTogglableElement

    if (target.isChecked()) {
      this.value = target.value
    } else {
      this.value = null
    }

    this.radiosEl.forEach((el) => {
      if (el.value !== target.value && el.isChecked()) {
        el.uncheck()
      }
    })

    this.checkError()
    this.touched = true
    this.dispatchEvent(new CustomEvent('input', { detail: this.value }))
  }

  protected refreshValue() {
    let value: string = ''
    for (const radioEl of this.radiosEl) {
      if (radioEl.isChecked()) {
        value = radioEl.value
      }
    }
    if (this.value !== value) {
      this.value = value
    }
  }

  protected setValue(value: string) {
    // this.value = value

    for (const radioEl of this.radiosEl) {
      if (this.value === radioEl.value) {
        radioEl.check()
      } else {
        radioEl.uncheck()
      }
    }
  }

  attributeChangedCallback(name: string, oldval: string, newval: string) {
    super.attributeChangedCallback(name, oldval, newval)

    if (name === 'value') {
      setTimeout(() => {
        this.setValue(newval)
      })
    }
  }

  checkError() {
    if (this.required && !this.value) {
      this.valid = false
    } else {
      this.valid = true
    }
  }

  public reset(): void {
    this.touched = false
    this.value = ''
  }

  public getModelValue(): any {
    return this.value
  }

  public setFocus(): void {
    this.radiosEl[0]?.focus()
  }

  public getValidity(): boolean {
    return !this.required || !!this.value
  }

  public checkValidity(): boolean {
    this.touched = true
    this.checkError()
    return this.getValidity()
  }
}
