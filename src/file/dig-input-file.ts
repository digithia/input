import {
  customElement,
  LitElement,
  css,
  html,
  property,
  TemplateResult,
  CSSResult,
  query,
  state,
} from 'lit-element'
import { ifDefined } from 'lit-html/directives/if-defined'

import commonStyle from '../common/dig-input-shared.scss'
import style from './dig-input-file.scss'

@customElement('dig-input-file')
export class DigInputFile extends LitElement {
  static styles = css`
    ${commonStyle as CSSResult}
    ${style as CSSResult}
  `

  /**
   * Enables mutiple file selection.
   */
  @property({ type: Boolean })
  multiple = false

  /**
   * Disables input file.
   */
  @property({ type: Boolean })
  disabled = false

  /**
   * Enables files filter.
   */
  @property({ type: String })
  accept = ''

  @state()
  files = []

  protected debounceTimeout?: NodeJS.Timeout

  @query('.dig-input-file-input')
  inputEl!: HTMLInputElement
  @query('slot')
  slotEl!: HTMLSlotElement

  render(): TemplateResult {
    return html`
      <slot @click="${this.toggle}"></slot>
      <input
        class="dig-input-file-input"
        type="file"
        files="${this.files}"
        ?multiple="${this.multiple}"
        ?disabled="${this.disabled}"
        .accept="${ifDefined(this.accept)}"
        @input="${this.handleInput}"
      />
    `
  }

  protected handleInput(event: any): void {
    this.getFiles(event.target.files)
  }

  protected getFiles(files: FileList): void {
    for (const file of files) {
      this.getFile(file)
    }
  }

  protected getFile(file: File): void {
    const fileReader = new FileReader()
    fileReader.readAsDataURL(file)
    fileReader.addEventListener('load', this.emitFile(file))
  }

  protected emitFile =
    (file: File) =>
    (event: ProgressEvent<FileReader>): void => {
      this.dispatchEvent(
        new CustomEvent('loaded', {
          detail: { file, base64: event.target?.result },
        })
      )
    }

  /**
   * Opens file selection menu
   */
  public toggle(): void {
    this.inputEl.click()
  }
}
