import {
  customElement,
  css,
  html,
  property,
  TemplateResult,
  state,
  CSSResult,
  query,
} from 'lit-element'
import { classMap } from 'lit-html/directives/class-map'
import { ifDefined } from 'lit-html/directives/if-defined'
import { DigUtils } from '../common/dig-utils'

import commonStyle from '../common/dig-input-shared.scss'
import style from './dig-input-password.scss'
import { DigInputElement, DigInputErrors } from '../common/dig-input'
import { DigInputConfig } from '../common/dig-input-config'

/**
 * Represents a custom password input field.
 * Extends DigInputElement and thus LitElement.
 * This custom Component handles password input. It can be configured with severals behaviors.
 */
@customElement('dig-input-password')
export class DigInputPassword extends DigInputElement {
  static styles = css`
    ${commonStyle as CSSResult}
    ${style as CSSResult}
  `

  /**
   * Input's current value. Changes within input are reflected to the DOM.
   */
  @property({ type: String, reflect: true })
  value: string = ''
  /**
   * Contains the placeholder shown within input if there is no text written and input is focused.
   */
  @property({ type: String })
  placeholder = ' '
  /**
   * Input's name. Use it in order to trigger browsers heuristics for autocompletion.
   */
  @property({ type: String })
  name = ''
  /**
   * Triggers required validator.
   */
  @property({ type: Boolean })
  required = false
  /**
   * Triggers pattern validator
   */
  @property({ type: String })
  pattern = undefined
  /**
   * Triggers min validator
   */
  @property({ type: Number })
  min = undefined
  /**
   * Triggers max validator
   */
  @property({ type: Number })
  max = undefined
  /**
   * Triggers minlength validator
   */
  @property({ type: Number })
  minlength = undefined
  /**
   * Triggers maxlength validator
   */
  @property({ type: Number })
  maxlength = undefined
  /**
   * Show an hint below the input field
   */
  @property({ type: String })
  hint = ''
  /**
   * Triggers HTML step.
   */
  @property({ type: String })
  step = 'any'
  /**
   * Triggers HTML inputmode. Use it to use the good keyboard on mobile.
   */
  @property({ type: String })
  inputmode = 'text'
  /**
   * Contains error messages which are displayed if needed.
   */
  @property({ type: Object })
  errors: DigInputErrors = {}
  /**
   * Selects the error behavior.
   */
  @property({ type: String, attribute: 'error-behavior' })
  errorBehavior = DigInputConfig.ERROR_BEHAVIOR
  /**
   * Add a custom validator to the input.
   */
  @property({ type: Function })
  validator?: (value: string) => boolean = undefined
  /**
   * Add a custom async validator to the input
   */
  @property({ type: Function })
  asyncValidator?: (value: string) => Promise<boolean> | boolean = undefined
  /**
   * Represents the time before errors will be displayed. Works only if errorBehavior is sets to 'debounce' (default)
   */
  @property({ type: Number, attribute: 'debounce-delay' })
  debounceDelay = DigInputConfig.ERROR_DELAY
  /**
   * Represents the time before the async validator is executed.
   */
  @property({ type: Number, attribute: 'async-debounce-delay' })
  asyncDebounceDelay = DigInputConfig.ERROR_DELAY
  /**
   * Activates or not the default browser autocomplete
   */
  @property({ type: Boolean })
  autocomplete = true

  @state()
  protected valid = true
  @state()
  protected type = 'password'

  protected errorTimeout?: NodeJS.Timeout
  protected asyncTimeout?: NodeJS.Timeout

  @query('.dig-input-password')
  inputEl!: HTMLInputElement

  render(): TemplateResult {
    return html`
      <dig-input-field
        ?show-validity="${this.showValidity}"
        ?valid="${this.valid && !!this.value}"
        ?invalid="${!this.valid}"
        ?focused="${this.focused}"
      >
        <input
          .id="${this.id}"
          .value="${ifDefined(this.value)}"
          .placeholder="${this.placeholder}"
          .type="${this.type}"
          .name="${this.name}"
          ?disabled="${this.disabled}"
          ?readonly="${this.readonly}"
          ?required="${this.required}"
          autocomplete="${this.autocomplete ? 'on' : 'off'}"
          .pattern=${ifDefined(this.pattern)}
          .min=${ifDefined(this.min)}
          .max=${ifDefined(this.max)}
          .minLength=${ifDefined(this.minlength)}
          .maxLength=${ifDefined(this.maxlength)}
          .step="${this.step}"
          .inputmode="${this.inputmode}"
          class="dig-input-password ${classMap({
            disabled: this.disabled || this.readonly,
          })}"
          size="5"
          @input="${this.handleInput}"
          @blur="${this.handleBlur}"
          @focus="${this.handleFocus}"
        />

        <dig-input-show-password
          ?shown="${true}"
          @toggle="${this.togglePassword}"
        ></dig-input-show-password>

        <dig-input-label
          ?asterisk="${this.required}"
          ?active="${!!this.value || this.focused || this.type === 'date'}"
          ?transparent="${(this.disabled || this.readonly) && !this.value}"
          @toggle="${this.setFocus}"
        >
          <slot></slot>
        </dig-input-label>
        <dig-input-hint ?hidden="${this.showValidity && !this.valid}">
          ${this.hint}
        </dig-input-hint>
        <dig-input-error ?shown="${this.showValidity && !this.valid}">
          ${this.errorMessage}
        </dig-input-error>
        <dig-input-hint
          class="maxlength"
          ?hidden="${this.maxlength === undefined}"
          position="right"
        >
          ${this.value?.length || 0} / ${this.maxlength}
        </dig-input-hint>
      </dig-input-field>
    `
  }

  firstUpdated(): void {
    this.checkError()
  }

  attributeChangedCallback(name: string, oldval: string, newval: string): void {
    super.attributeChangedCallback(name, oldval, newval)
    if (name === 'readonly') {
      setTimeout(() => {
        if (newval || newval === '') {
          this.inputEl.setAttribute('tabindex', '-1')
        } else {
          this.inputEl.removeAttribute('tabindex')
        }
      })
    } else if (name === 'value') {
      setTimeout(() => {
        this.handleValue()
        if (this.errorBehavior === 'immediate') {
          this.checkError()
        } else if (this.errorBehavior === 'debounce') {
          this.errorTimeout = DigUtils.debounce(
            this.debounceDelay,
            this.errorTimeout,
            () => this.checkError()
          )
        }
      })
    }
  }

  protected handleInput(): void {
    this.touched = true
    this.handleValue()
  }

  protected handleBlur(): void {
    this.focused = false
    if (this.errorBehavior === 'blur') {
      this.checkError()
    }
  }

  protected handleFocus(): void {
    this.focused = true
  }

  protected handleValue(): void {
    this.value = this.inputEl?.value || ''
    this.valid = true
  }

  protected checkError(): void {
    if (this.errorBehavior === 'none') return

    let errorMessage = null

    if (this.inputEl?.validity.valueMissing) {
      errorMessage = this.errors.required
    } else if (this.inputEl?.validity.patternMismatch) {
      errorMessage = this.errors.pattern
    } else if (this.inputEl?.validity.rangeUnderflow) {
      errorMessage = this.errors.min
    } else if (this.inputEl?.validity.rangeOverflow) {
      errorMessage = this.errors.max
    } else if (this.inputEl?.validity.tooShort) {
      errorMessage = this.errors.minlength
    } else if (this.inputEl?.validity.tooLong) {
      errorMessage = this.errors.maxlength
    } else if (this.validator) {
      if (!this.validator(this.inputEl?.value || '')) {
        this.inputEl?.setCustomValidity(
          this.errors.validator || DigInputConfig.ERROR_MESSAGE
        )
      } else {
        this.inputEl?.setCustomValidity('')
      }
    } else if (this.asyncValidator) {
      this.inputEl?.setCustomValidity('')
      this.asyncTimeout = DigUtils.debounce(
        this.asyncDebounceDelay,
        this.asyncTimeout,
        async () => {
          if (
            this.asyncValidator &&
            !(await this.asyncValidator(this.inputEl?.value || ''))
          ) {
            this.inputEl?.setCustomValidity(
              this.errors.validator || DigInputConfig.ERROR_MESSAGE
            )
          } else {
            this.inputEl?.setCustomValidity('')
          }
          this.errorMessage = this.inputEl?.validationMessage
          this.valid = this.inputEl?.validity.valid
        }
      )
    }

    this.errorMessage = errorMessage || this.inputEl?.validationMessage
    this.valid = this.inputEl?.validity.valid
  }

  protected togglePassword(): void {
    if (this.type === 'password') {
      this.type = 'text'
    } else {
      this.type = 'password'
    }
  }

  public reset(): void {
    this.value = ''
    this.touched = false
    this.valid = true
  }

  public getModelValue(): string {
    let modelValue = this.value
    return modelValue
  }

  public getValidity(): boolean {
    return this.valid
  }

  public checkValidity(): boolean {
    this.touched = true
    this.checkError()
    return this.valid
  }
}
