# Digithia Input

<img src="digithia.jpg" alt="Digithia" width="200" />

This library provides ready to use input fields. It use `lit-element` to create native web components.

Disclaimer : this library is not ready for production yet. Breaking changes are expected and some components may not works as expected.

[![pipeline status](https://gitlab.com/digithia/input/badges/master/pipeline.svg)](https://gitlab.com/digithia/input/commits/master)
[![coverage report](https://gitlab.com/digithia/input/badges/master/coverage.svg)](https://gitlab.com/digithia/input/commits/master)

## Install

### With NPM

If your project is setuped with npm, you just have to run this command :

`npm install @digithia/input`

### On simple HTML

If you want to import directly into an HTML file :

```html
<!-- index.html -->
<html>
  ...
  <body>
    ...
    <script src="https://unpkg.com/@digithia/input" defer></script>
  </body>
</html>
```

## Use

In order to import this lib, you have to copy this in your project :

```js
// index.js
import '@digithia/input' // Imports all components
```

The above line import all components to your project, but some components are pretty heavy. So you could use the following syntax to only import needed components :

```js
// index.js
import '@digithia/input/text' // Imports only dig-input-text component
import '@digithia/input/select' // Imports only dig-input-select-picker and dig-input-select-picker-option components
...
```

Once imported, you could simply use them in your template like native html :


```html
<!-- index.html -->
<body>
  ...
  <dig-input-text value="my value">My label</dig-input-text>
  ...
</body>
```

### Typescript

This lib is written entierly in Typescript so you can retrieve its types.

```ts
import { DigInputText, DigInputRadio } from '@digithia/input'
// Or
import { DigInputText } from '@digithia/input/text'
import { DigInputRadio } from '@digithia/input/radio'

const inputText = document.querySelector('dig-input-text') as DigInputText
inputText.hint = 'My text hint'
const inputRadio = document.querySelector('dig-input-radio') as DigInputRadio
inputRadio.value = 'My radio value'
```

## Behaviors

You might want to enable some build in functionalities. You can enable them simply with an attribute :

```html
<dig-input-text value="my value" hint="My hint">My label</dig-input-text>
```

will show an hint below text field.

Same in a js file :

```js
const input = document.querySelector('dig-input-text')
input.hint = 'My hint'
```

Some attribute are interactive. Whenever you type into this field, the `value` attribute will reflects changes.

This text field will be filled with "another value" but everytime you type in the field, value attribute will reflect your changes.

```html
<dig-input-text value="another value">My label</dig-input-text>
```

You can also listen to the events sent by the input field like this :

```js
const input = document.querySelector('dig-input-text')
input.addEventListener('blur', (event) => {
  alert('You leave the field with the value : ', event.target.value)
})
```

With a framework like Vue, you could use event binding like so :

```html
<dig-input-text value="another value" @blur="myFunction">My label</dig-input-text>
```

## Styles

Each component uses css variables to handle its own style. You can override them to style your components :

```css
dig-input-text {
  --dig-input-text-font-size: 22px;
  --dig-input-text-border-color: orange;
  ...
}
```

By modifing css variables, the complete design should be preserved, thanks to css calculations. In this example, changing input font size will also adapt input height, thus the text will not overflow.

## More

For further information, go to the git page and look at `doc/` folder or `e2e/` tests.
