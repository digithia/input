import * as webpack from 'webpack'
import { Configuration } from 'webpack'
import path from 'path'
import CopyPlugin from 'copy-webpack-plugin'
import { CleanWebpackPlugin } from 'clean-webpack-plugin'
import { merge } from 'webpack-merge'
import commonConfig from './webpack-common.config'

const config: Configuration = merge(commonConfig, {
  mode: 'production',
  entry: {
    index: [
      './vendors.js',
      './vendors-phone.js',
      path.resolve(__dirname, './src/index.ts'),
    ],
    'common/index': [
      '../vendors.js',
      path.resolve(__dirname, './src/common/index.ts'),
    ],
    'checkbox/index': [
      '../common/index.js',
      path.resolve(__dirname, './src/checkbox/index.ts'),
    ],
    'file/index': [
      '../common/index.js',
      path.resolve(__dirname, './src/file/index.ts'),
    ],
    'number/index': [
      '../common/index.js',
      path.resolve(__dirname, './src/number/index.ts'),
    ],
    'password/index': [
      '../common/index.js',
      path.resolve(__dirname, './src/password/index.ts'),
    ],
    'phone/index': [
      '../vendors-phone.js',
      '../common/index.js',
      path.resolve(__dirname, './src/phone/index.ts'),
    ],
    'radio/index': [
      '../common/index.js',
      path.resolve(__dirname, './src/radio/index.ts'),
    ],
    'select/index': [
      '../common/index.js',
      path.resolve(__dirname, './src/select/index.ts'),
    ],
    'text/index': [
      '../common/index.js',
      path.resolve(__dirname, './src/text/index.ts'),
    ],
    'textarea/index': [
      '../common/index.js',
      path.resolve(__dirname, './src/textarea/index.ts'),
    ],
  },
  externals: {
    './vendors.js': './vendors.js',
    '../vendors.js': '../vendors.js',
    './vendors-phone.js': './vendors-phone.js',
    '../vendors-phone.js': '../vendors-phone.js',
    '../common/index.js': '../common/index.js',
  },
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: '[name].js',
    library: 'digithiaInput',
    libraryTarget: 'umd',
    umdNamedDefine: true,
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        lit: {
          test: /node_modules\/lit-*/,
          name: 'vendors',
          chunks: 'all',
          reuseExistingChunk: true,
          enforce: true,
        },
        phone: {
          test: /node_modules\/countries-list|node_modules\/google-libphonenumber/,
          name: 'vendors-phone',
          chunks: 'initial',
          reuseExistingChunk: true,
          enforce: true,
        },
      },
    },
  },
  plugins: [
    new CleanWebpackPlugin(),
    new CopyPlugin({
      patterns: [
        {
          from: path.resolve(__dirname, './package.json'),
          to: path.resolve(__dirname, './dist/package.json'),
        },
        {
          from: path.resolve(__dirname, './README.md'),
          to: path.resolve(__dirname, './dist/README.md'),
        },
      ],
    }),
  ],
})

export default config
