# Digithia Input

## 🔧 Build the project

Run `npm run build` to build the project. Resulting code will be located into `dist/` folder.

In order to build project into developement mode (watch files), you have to run `npm run dev`. This command build the tester of the app located at `e2e/tester/` into `e2e/dist/`

Build is done with webpack. You could see webpack the common config into `webpack-common.config.js` file. See `webpack-lib.config.js` and `webpack-tester.config.js` for differences between build and dev mode.

## ✅ Test

`karma` & `chai` are used to perform tests. `nyc` is used to make the coverage report of all tests. Each test file is positioned next to his source file.

## 🏛 Architecture

- `doc/` : You are here ! This folder contains all documentation for this project.
- `e2e/` : For testing and developement
  - `tester/` : The tester code
  - `tests/` : end to end tests
- `src/` Main code
  - `common` : A folder which contains the common code for all components
  - `components` : A folder which contains all components. Each components, or group of components have to be into a specific folder
