# Usage with git

This package use Git and GitLab.

## Branches

Repository has 3 main and protected banches :

- Master
- Release
- Dev

Master is the current code available on npmjs.com.
Release purpose is to tag code as a version
Dev is for all other case

## Flow

GitLab is configurated with Continuous Integration and Continuous Delivery (CI/CD). Pipelines are defined in .gitlab-ci.yml file and have the following stages.

### 1) Install

Install all dependencies with a `npm install`. `node_modules` generated is cache and used by other jobs.

### 2) Build

Build the project with webpack and create an art (.gz) of the `dist/` folder generated. Archive is then upload to an artifact which is used by other jobs.

### 3) Test

Run project's tests and create a coverage report.

### 4) Deploy

Get build artifact, unzip it and add npm key. Then the package is published on npmjs. Also publish coverage report to the GitLab's Pages so they became availables for the README.

## Commit message chart

In order to be clear and readable, commit message should always be structured like this :

```
😀 Consice title
- Do this
- And that
```

Keep an icon in the title as often as possible to improve commit readability.

Here are some predefined titles with their emoticon you can use :

- 🚀 Release vx.x.x
- 🎁 New functionality
- 🚑 Fix bug
- 🤖 Test
- ✅ Coverage
- 🧹 Refactor
- 📝 Documentation
- ⛈ Breaking changes
- ⚙️ CI/CD
- 🔧 Quick fix
- 📰 Update dependencies
- 🎨 Update style
- 🔒 Security
- 🏎 Perfomance
- 🏛 Architecture

## Bests practices

Please follow this architecture model for all `lit-element` components :

```
{
  css
  properties
  internalProperties
  getter/setter
  queries
  render html
  other lit element functions
  protected functions
  public functions
}

```
