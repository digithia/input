import { Selector } from 'testcafe'

fixture('Inputs').page`http://127.0.0.1:1234/index.html`

const inputEl = Selector('#username input')

test('input element should be loaded', async (t) => {
  await t.expect(inputEl.innerText).ok
})
