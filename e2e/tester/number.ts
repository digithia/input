import '@src/index'
import { DigInputNumber } from '@src/number'

const input = document.querySelector('#input') as DigInputNumber

const textSpan = document.querySelector('#text') as HTMLSpanElement

const buttonSetValue = document.querySelector(
  '#btn-set-value'
) as HTMLButtonElement
const buttonSetFocus = document.querySelector(
  '#btn-set-focus'
) as HTMLButtonElement
const buttonCheckValidity = document.querySelector(
  '#btn-check-validity'
) as HTMLButtonElement
const buttonReset = document.querySelector('#btn-reset') as HTMLButtonElement
const buttonGetValue = document.querySelector(
  '#btn-get-value'
) as HTMLButtonElement
const buttonDisabled = document.querySelector(
  '#btn-disabled'
) as HTMLButtonElement
const buttonReadonly = document.querySelector(
  '#btn-readonly'
) as HTMLButtonElement

buttonSetValue?.addEventListener('click', () => {
  input.value = 280.54
})

buttonSetFocus?.addEventListener('click', () => {
  input.setFocus()
})

buttonCheckValidity?.addEventListener('click', () => {
  input.checkValidity()
})

buttonReset?.addEventListener('click', () => {
  input.reset()
})

buttonGetValue?.addEventListener('click', () => {
  textSpan.textContent = `${input.getModelValue()}`
})

buttonDisabled?.addEventListener('click', () => {
  if (input.disabled) {
    input.removeAttribute('disabled')
  } else {
    input.setAttribute('disabled', '')
  }
})

buttonReadonly?.addEventListener('click', () => {
  if (input.readonly) {
    input.removeAttribute('readonly')
  } else {
    input.setAttribute('readonly', '')
  }
})
