import '@src/index'
import { DigInputSelectPicker } from '@src/select'

const select = document.querySelector('#select') as DigInputSelectPicker

const textSpan = document.querySelector('#text') as HTMLSpanElement

const buttonSetValue = document.querySelector(
  '#btn-set-value'
) as HTMLButtonElement
const buttonSetinvalidValue = document.querySelector(
  '#btn-set-invalid-value'
) as HTMLButtonElement
const buttonSetFocus = document.querySelector(
  '#btn-set-focus'
) as HTMLButtonElement
const buttonCheckValidity = document.querySelector(
  '#btn-check-validity'
) as HTMLButtonElement
const buttonReset = document.querySelector('#btn-reset') as HTMLButtonElement
const buttonGetValue = document.querySelector(
  '#btn-get-value'
) as HTMLButtonElement

buttonSetValue?.addEventListener('click', () => {
  select.value = 'meat'
})

buttonSetinvalidValue?.addEventListener('click', () => {
  select.value = 'Not in the list'
})

buttonSetFocus?.addEventListener('click', () => {
  select.setFocus()
})

buttonCheckValidity?.addEventListener('click', () => {
  select.checkValidity()
})

buttonReset?.addEventListener('click', () => {
  select.reset()
})

buttonGetValue?.addEventListener('click', () => {
  textSpan.textContent = select.getModelValue()
})

// select.addEventListener('select', (event) => {
//   console.log('select', (event as CustomEvent).detail)
// })
