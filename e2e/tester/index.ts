import {
  DigInputConfig,
  DigInputController,
  DigInputPassword,
  DigInputPhone,
} from '@src/index'
DigInputConfig.ERROR_DELAY = 1000

import '@src/text'
import '@src/select'
import '@src/file'
import '@src/phone'
import '@src/radio'
import '@src/checkbox'
import '@src/textarea'

import { DigInputText } from '@src/text'
import { DigUtils } from '@src/common/dig-utils'

const inputUsername = document.querySelector('#username') as DigInputText

inputUsername.asyncValidator = async (value) => {
  return await new Promise((resolve) => {
    setTimeout(() => {
      resolve(!['john', 'josh', 'joe'].find((u) => u === value))
    }, 500)
  })
}
inputUsername.errors.validator = 'This username is already taken'

const inputPassword = document.querySelector('#password') as DigInputPassword

inputPassword.validator = (value) => !!value && value.length > 8
inputPassword.errors.validator = 'Your password is not long enough'

const inputPasswordConf = document.querySelector(
  '#password-conf'
) as DigInputPassword

inputPasswordConf.validator = (value) => {
  return value === inputPassword.value
}
inputPasswordConf.errors.validator =
  'Your password confirmation not match password'

document.getElementById('show-result')?.addEventListener('click', (e) => {
  e.preventDefault()
  const formWrapper = document.getElementById('form-wrapper') as HTMLElement
  let result = ''
  for (const component of formWrapper.children) {
    result += (component as any).getModelValue() + ' '
  }
  const el = document.getElementById('result') as HTMLElement
  el.innerHTML = result
})

const inputFile = document.querySelector('.dig-input-file')
inputFile?.addEventListener('loaded', (event: any) => {
  console.log(event.detail)
})

const inputIban = document.querySelector('#iban') as DigInputText
inputIban.validator = DigUtils.checkIBAN
inputIban.errors.validator = 'Your IBAN is invalid'

const inputPhone = document.querySelector('#phone') as DigInputPhone
inputPhone.errors.invalid = `Le téléphone est invalide !`

setTimeout(() => {
  inputPhone.setAttribute('default-country', 'it')
}, 1500)

const btnCheck = document.querySelector('#btn-check')
btnCheck?.addEventListener('click', () => {
  console.log(DigInputController.checkInputsValidity('form'))
})
const btnInit = document.querySelector('#btn-init')
btnInit?.addEventListener('click', () => {
  DigInputController.resetInputsValidity('form')
})
