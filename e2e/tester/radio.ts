import '@src/radio'

const radioFruits = document.querySelector('#radio-fruits')

setTimeout(() => {
  const newChild = document.createElement('dig-input-radio')
  newChild.innerText = 'Peach'
  newChild.setAttribute('value', 'peach')
  newChild.setAttribute('checked', '')
  radioFruits?.appendChild(newChild)
}, 2000)

setTimeout(() => {
  radioFruits?.setAttribute('value', '3')
}, 4000)
