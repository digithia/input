import '@src/index'
import { DigInputPhone } from '@src/phone'

const input = document.querySelector('#input') as DigInputPhone

const textSpan = document.querySelector('#text') as HTMLSpanElement

const buttonSetValue = document.querySelector(
  '#btn-set-value'
) as HTMLButtonElement
const buttonSetValueInternational = document.querySelector(
  '#btn-set-value-international'
) as HTMLButtonElement
const buttonSetFocus = document.querySelector(
  '#btn-set-focus'
) as HTMLButtonElement
const buttonCheckValidity = document.querySelector(
  '#btn-check-validity'
) as HTMLButtonElement
const buttonReset = document.querySelector('#btn-reset') as HTMLButtonElement
const buttonGetValue = document.querySelector(
  '#btn-get-value'
) as HTMLButtonElement
const buttonDisabled = document.querySelector(
  '#btn-disabled'
) as HTMLButtonElement
const buttonReadonly = document.querySelector(
  '#btn-readonly'
) as HTMLButtonElement

buttonSetValue?.addEventListener('click', () => {
  input.value = '0123456789'
})

buttonSetValueInternational?.addEventListener('click', () => {
  const rand = Math.random()
  if (rand < 0.25) {
    console.log('GABON !')
    input.value = '+241 06 58 78 02'
  } else if (rand < 0.5) {
    console.log('FRANCE !')
    input.value = '+33 1 23 45 67 89'
  } else if (rand < 0.75) {
    console.log('GERMANY !')
    input.value = '+49 30 238923'
  } else if (rand < 0.95) {
    console.log('LUX !')
    input.value = '+352 661199941'
  } else if (rand < 1) {
    console.log('UK !')
    input.value = '+44 302 389 2323'
  }
})

buttonSetFocus?.addEventListener('click', () => {
  input.setFocus()
})

buttonCheckValidity?.addEventListener('click', () => {
  input.checkValidity()
})

buttonReset?.addEventListener('click', () => {
  input.reset()
})

buttonGetValue?.addEventListener('click', () => {
  textSpan.textContent = `${input.getModelValue()}`
})

buttonDisabled?.addEventListener('click', () => {
  if (input.disabled) {
    input.removeAttribute('disabled')
  } else {
    input.setAttribute('disabled', '')
  }
})

buttonReadonly?.addEventListener('click', () => {
  if (input.readonly) {
    input.removeAttribute('readonly')
  } else {
    input.setAttribute('readonly', '')
  }
})
