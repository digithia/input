import '@src/text'
import { DigInputText } from '@src/text'

const input = document.querySelector('#input') as DigInputText

const textSpan = document.querySelector('#text') as HTMLSpanElement

const buttonSetValue = document.querySelector(
  '#btn-set-value'
) as HTMLButtonElement
const buttonSetFocus = document.querySelector(
  '#btn-set-focus'
) as HTMLButtonElement
const buttonCheckValidity = document.querySelector(
  '#btn-check-validity'
) as HTMLButtonElement
const buttonReset = document.querySelector('#btn-reset') as HTMLButtonElement
const buttonGetValue = document.querySelector(
  '#btn-get-value'
) as HTMLButtonElement

buttonSetValue?.addEventListener('click', () => {
  input.value = 'something'
})

buttonSetFocus?.addEventListener('click', () => {
  input.setFocus()
})

buttonCheckValidity?.addEventListener('click', () => {
  input.checkValidity()
})

buttonReset?.addEventListener('click', () => {
  input.reset()
})

buttonGetValue?.addEventListener('click', () => {
  textSpan.textContent = input.getModelValue()
})
